[back to index](./README.md)
# Notes about the CLI (command line interface)
* [VIM](./notes-cli-vim.md) : _some notes about a really nice CLI text editor_
## Bash commands
* `cd` will change directories (current folder)
* `ls` will list contents of current directory (`ls -all` will show more info)
* `man [command]` will open the manual for the command you specified
* `whatis [command]` tells you what the specified command means
* `ssh` is remote login

## Extra notes
### "Terminal froze when I tried to save file"
Many might have the automatic reflex of using `ctrl + S` to save a file and tempted to use it with any editor, but everything seems to freeze if done in a terminal. 

Apparently using `ctrl + S` is like to telling the terminal to __stop__ everything. To _unfreeze_ the terminal in these cases, use `ctrl + Q` 
