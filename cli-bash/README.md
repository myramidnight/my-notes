# Command Line Interface (CLI)
The very basic way to interact with the computer is through the Command line interface. It used to be the only way until we got our fancy Graphical User Interfaces.

If you have a virtual server, then you would only be able to communicate with it through a CLI (unless you had a remote desktop or something of the sort).

* __Terminal__ = __tty__ = text input/output enviornment
* __console__ = physical terminal
* __shell__ = command line interpreter


### Scripting languages
* BASH
    > It is the linux/unix command terminal
* Command Prompt (CMD)
    > It is the windows equivalent of bash
* PowerShell 
    > It is a advanced version of CMD
## Index 
* ### [Command Line Interface](cli.md)
* ### [VIM text editor](cli-vim.md)
* ### [JustFile](justfile/README.md)
* ### [shell-script](shell-script/README.md)
## Other things
* [Commandlinefu.com](https://www.commandlinefu.com/commands/browse/sort-by-votes)
    >Need to write a bash command that might exist? here you can find the CLI gems