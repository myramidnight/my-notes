[back to index](../README.md)

# VIM text editor for the CLI
There are many text editors available, most are very basic when it comes to CLI text editors but the `VIM` editor is highly praised for being quite advanced as far as CLI editors go.

VIM isn't so scary once you get the hang of things, all you need to know is how to enter and exit `insert mode`, and a few commands.

> Simply run `vim file_name` to open a file with `VIM` editor.

## The basics in `VIM`
By default you enter `VIM` in `normal mode` when you open your file, which means you will need to enter `insert mode` to edit the text.
* The normal arrow keys will move your _cursor_ through the text
* Pressing `i` will enter `insert mode`, where you can use the keyboard to edit the file just like you would expect in any other text editor.
* Pressing `ESC` will go back to `normal mode`

### Commands in `VIM`
Just write them in the terminal when in `normal mode`
* `:q` or `:quit` will quit the program (if no changes were made)
* `:q!` or `:quit!` will quit the program without saving 
* `:w` or `:up` will save/update the file without exiting the editor
* `:wq` or `:x` will save and exit the editor
* `:w new_filename` will save as a new file
* `:h` will enter the help info
* `:e file_name` opens a file to edit

## More advanced things in `VIM`
### Normal mode
These are things to do while in `normal mode`
* Pressing the `khjl` keys will move the _cursor_ through the text 
  (similar to the `wasd` that you are used to in games). 
* pressing `w` or `e` moves you to the __right__ by jumping to the first/last letter in each word.
* pressing `b` or `B` moves you to the __left__ by jumping to the beginning of words.

Interacting with text while in `normal mode`
* Pressing `x` or `delete` will delete text
* Pressing `u` will undo changes
* `ctrl + r`will redo any changes that were undone.
* Numbered  navigation: `3w` would be same as pressing `w` three times.
* Inserting with numbers: `30i -` would insert thirty dashes, `3i go` would insert `gogogo` (all without entering `insert mode`)


### Visual mode
You can enter `visual mode` by pressing `V` when in `normal mode`, and by pressing `v` you can select single characters.

Once the text is highlighted, you can:
* Press `d` to delete it.
* Press `y` to copy (yank) the text.
* Press `p` to paste it.

### Keyword search
Vim has a feature that highlights a keyword when in `normal mode` and they stay highlighted after you switch modes.
* simply write `/` followed by a keyword or phrase and then press `enter`. 

The editor will remember your search even on later sessions, so to clear the highlighting just write `:noh`

## Extra notes:
> A fun way to learn how to properly use this editor: [VIM adventures](https://vim-adventures.com/)
### Frozen terminal
Many might have the automatic reflex of using `ctrl + S` to save a file and tempted to use it with any editor, but everything seems to freeze if done in a terminal. 

Apparently using `ctrl + S` is like to telling the terminal to __stop__ everything. To _unfreeze_ the terminal in these cases, use `ctrl + Q` 
