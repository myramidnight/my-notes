# RabbitMQ
This is a message broker. It is compatible with any programming language. 

Message brokers handle subscriptions and letting the consumers know when events arrive. This is used for communication between microservices.

## Install RabbitMQ
* [Get installer for your OS](https://www.rabbitmq.com/download.html)
* It will want you to install [Erlang](https://www.erlang.org/downloads)

### Setting up server
1. Open the `RabbitMQ Command Prompt` that comes with the install.
2. Run the following command in the RabbitMQ CLI
>```
>rabbitmq-plugins.bat enable rabbitmq_management
>``` 
3. You can then log into the server with user `guest` and password `guest`
4. To disable the server, run the following command
>```
>rabbitmq-plugins.bat disable rabbitmq_management
>```

```
http://localhost:15672/
```

## AMQP client
This is usally what you install to communicate with rabbitMQ. It will usually listen for any messages on the ports `5672` and `5671`.