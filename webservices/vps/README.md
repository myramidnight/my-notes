# VPS (Virtual Private Server)
These are just notes from my experience of setting up and running my first __VPS__, since I finally canceled my _shared hosting_ to have my own server running. 
* It was alot cheaper than my _unlimited_ shared hosting plan I had previously
* Having a VPS means you are responsible for it, that things work within the server
    * congrats, you're a real webmaster now!
* It was alot less intimidating to start running a VPS after learning a bit about computer science. My forte before was mainly creating websites, not how the computer worked.
    * When you realise it's just a remote computer, and learned to interact with computers through a terminal. Sure you can setup something so you can visually interact with the VPS, but you always have to start by using a terminal to set that up.

## Index
* ### [Basics](./basics.md)
    * Connecting to server 
    * Getting list of services 
    * Installing services/packages
    * How to restart services
* ### [Managing users](./users.md)
    >Sometimes you need to have other users than the __root__ account (for security)
    * Create users / groups
    * See list of groups and users
* ### [Apache server](./apache/README.md)
    > Your classic webserver
    * Setting up and using Apache
    * Managing PHP
        * The __PHP-FPM__ tool (to manage PHP versions)
    * Managing domains
* ### [Adding a database](./database/README.md)
* ### [Handling files](files.md)

* ### Adding a mailserver
