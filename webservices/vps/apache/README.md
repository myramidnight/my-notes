# Apache Webserver
How we host websites, specially shared hosting.

##  Index
* ### [Apache basics](apache-basics.md)
    * setting up the apache server
    * apache commands
* ### [Adding PHP](php.md)
    * Getting the PHP info
    * The __PHP-FPM__ tool
* ### [Domains](domain.md)
    * How to configure the virtual domains
    * Adding a SSL certificate (_https_ for secure connections)