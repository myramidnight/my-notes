

## Adding a new domain in Apache server
1. Go to `etc/apache2`
1. Locate the `sites-available`
1. Create a new file `yourdomain.conf`
1. Add the details to about the domain
    ```
    <VirtualHost *:80>
        ServerAdmin admin@example.com
        ServerName example.com
        ServerAlias www.example.com
        DocumentRoot /var/www/example.com/public_html
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
    </VirtualHost>
    ```
1. Enable the new site 
    ```
    a2ensite yourdomain.conf
    ```
1. Restart the apache server.

## SSL certificate
> https://letsencrypt.org/about/

In order to increase security on the internet, the _let's encrypt_ organization is providing a free way for people to gain a simple SSL certifacate. This makes interactions with your website more secure for people as things get encrypted.

The recomended way to get a SSL certificate is to use the _CertBot_. It gets you the SSL and adds it to your Apache server for you, very nice.


> When I used the _CertBot_ to add my certifacate to server, it added the following code within  `VirtualHost` to redirect traffic through the secure domain. Look at the notes about `SSL certifacate` in section.
```
    RewriteEngine on
    RewriteCond %{SERVER_NAME} =www.example.com [OR]
    RewriteCond %{SERVER_NAME} =example.com
    RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
```