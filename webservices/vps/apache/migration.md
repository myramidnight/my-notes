# Migrating between servers
When migrating your content/websites between servers, it is important to keep in mind what the requirements were in order for the site to run.

## PHP versions
A new server might have different versions of `PHP` or datbases that are not compatable. 

If you were migrating your content only to realize the php wasnt compatable, you might want to upgrade your site in original location first and then move things over. 

### Pay attention to date of modification
Useful way to avoid having to copy everything over again is to view the _last modified_ date of the files on the ftp. The date is a hint to what files were actually changed during the upgrade, and therefor those are the only files you need to copy and paste over to upgrade your migrated installation. Saves you alot of time.

All copied files will be newer, regardless of what their date was on the server, so if you copy everything and set it to only override if the source is newer, it will override everything anyway. So you will have to just pick out the files and folders first.