# PHP

## View Configurations

You can always create a `test.php` file to view all the PHP settings and configuration. Just add the following to the content and load the page 
```php
<?php  phpinfo(); ?>
```
> just remember to not leave this page accessable to avoid exposing your configuration in detail, or simply remove it when you're done

## PHP-FPM
This is actually a popular tool to manage PHP on servers, as it also allows you to run different versions of PHP for different users. 

I was actually having issues with the changes to the `php.ini` registering, and ended up having to install and use `php-fpm` as the Server API (can be seen listed if you use `phpinfo()`). 

Apparently  this is a tool to manage php on servers. 
> https://www.server-world.info/en/note?os=Ubuntu_20.04&p=httpd&f=7

> https://www.linuxbabe.com/ubuntu/install-lamp-stack-ubuntu-20-04-server-desktop

I already had php7.4 installed and connected to the Apache server. I had to disable it from the server to use this `php-fpm` instead. 
```
a2dismod php7.4
```

Main reason for choosing this option for me was that all the trouble shooting suggestions said to restart php with `php-fpm` if all else failed, which implies you would have to be using `php-fpm` in the first place.

## PHP commands in terminal
### Check version of PHP
*
    ```
    php -v
    ```
* or
    ```
    php --version
    ```
### Check what PHP modules are installed on the server
```
php -m
```