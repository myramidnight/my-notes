# Mail server

## Creating the hostname for mail server
* [Setup postfix mail server](https://www.tecmint.com/setup-postfix-mail-server-in-ubuntu-debian/)
* [Does the hostname matter?](https://serverfault.com/questions/91943/does-it-really-matter-what-your-servers-hostname-is)

You should probably look at the original guides and try to follow those, and if you get confused, then maybe my notes here might help, but I am not really making a tutorial.

1. Just check what your current hostname is with the following command
    ```
    hostnamectl
    ```
1. Setting the hostname to have `mail` prefix
    ```
    hostnamectl set-hostname mail.domain.com
    ```
1. Now we can go to the domain register (usually found from the control panel of your service provider where you bought your domain from) and create the `A` and `MX` records.
    1. First the `A` record, where host `mail` directs to the ip address
    1. next we add the `MX` record, where host is `@` and we direct it at `mail.domain.com`