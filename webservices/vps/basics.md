
# Connecting to the server
You have to enter the server somehow
>Just had to write open the host address with ssh
>```
>ssh root@hostIp
>```
>Since I provided the hosting with my ssh key, it connected automatically (no password needed).

## Checking system
### See how much space is available
>The `df` command (abbrevation for `disk free`) will display the free space you have on your server disk. The following command will make the output sizes _human readable_.
>```
>df -h
>```
### Information about the OS
>Finding out the system information, such as OS and word-size.
>```
>uname -a
>```

## Installing packages
What I've installed, because I plan on having a few static websites.
* `apt install ufw` (uncomplicated fire wall)
    > [Setting up a UFW firewall](https://www.digitalocean.com/community/tutorials/how-to-set-up-a-firewall-with-ufw-on-ubuntu-20-04)
* `apt install apache2` (for apache server)
    > [Install apache web server on Ubuntu 20.04](https://www.digitalocean.com/community/tutorials/how-to-install-the-apache-web-server-on-ubuntu-20-04)
* `apt install nodejs` (because NodeJS is a must!)

## Using the `Systemctl` 
### See what services are running
You can always ask the system to list all the services on the computer. This is kind of like the _task manager_ on windows.
```
systemctl list-units --type=service --state=running 
```

When there are too many services to list on a single _page_, then you might have to use the arrow keys to scroll further down (just press `q` to exit this viewing mode if it happens), or just tell it with the `--no-pager` option to just print out everything at once. Can always pipe the output to a text file if looking at it in terminal isn't working out (perhaps it's displaying things weirdly when you want to scroll back up).

```
systemctl list-units --all --type=service --no-pager
```
> It is a good possible to filter the list to just show what you are looking for by using `grep`. 
```
systemctl list-units --all --type=service --no-pager | grep running
systemctl list-units --all --type=service --no-pager | grep exited
systemctl list-units --all --type=service --no-pager | grep dead
```

### Enable/Disable vs Start/Stop
These two command pairs seem similar, but they do different things. 
* `enable` and `disable` specifies what services will start up when you boot up the system
* `start` and `stop` will simply start or stop the service.

## Restarting services
```
service apache2 restart
```

## Updating and Upgrading services
Usually security depends on us having things up to date
```
apt update && apt upgrade
```
* It usually asks you if you want to proceed if it needs to install new things with the process

```
php7.4-common php-imagick php7.4-imap 
```