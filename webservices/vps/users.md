# How to manage users on the terminal
Since `root` is like the admin user, might want to have sepate user accounts to manage things.
## Create new user

```
adduser mynewuser
```
It will prompt you to fill extra info, but that's all optional. 

### Checking if user was created
* to list all local users that you can use
    ```
    cut -d: -f1 /etc/passwd
    ```
* to list all users
    ```
    cat /etc/passwd
    ```
* to single out by name (replace `mynewuser` with the name you are seraching for)
    ```
    grep '^mynewuser' /etc/passwd
    ```

## Adding user to superuser (sudo) group
This group gives administrative powers to users within the group. the `Root` user is the inital admin and therefor does not need the `sudo` command. For security reasons you might want to have alternative users than the root, specially if you're not using SSH keys for authentication
```
usermod -aG sudo mynewuser
```
### Testing `sudo` command
The new user might not have access to things, but with the `sudo` command, we can see things hidden to us, such as the root files. Will be promted for password on the inital `sudo` command to verify you.
```
sudo ls -la /root
```

## Create groups and adding users to the groups
1. Create the group
    ```
    addgroup mynewgroup
    ```
1. Add user to group
    ```
    adduser mynewuser mynewgroup
    ```
1. Give group access to folder `/home/www`
    ```
    chgrp -R mynewgroup /home/www
    ```
1. Set permissions on folder
    ```
    chmod -R 775 /home/www
    ```
1. Make all subfolders created inside `/home/www` belong to the group `mynewgroup`
    ```
    setfacl -dR -m g:mynewgroup:rwx /home/www
    ```

### See list of groups and it's members
```
less /etc/group
```
or the groups that a specific user belongs to
```
groups mynewuser
```