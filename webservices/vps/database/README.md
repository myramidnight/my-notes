# Databases on VPS
There many databases out there to use with our webservices.

We can then add a GUI (graphical user interface) on the webservice we set up, to make it easier to manage and interact with the database

You can find more detailed notes about the usage of these databases in the [__data-storage__](../../../data-storage/README.md) directory

## Setup
* ### [PostgreSQL](postgresql.md) 
    >I actually gave up on converting my existing websites to use this database type, I was having issues with Wordpress.
    * PgAdmin4 GUI
* ### MySQL / MariaDB
    > I actuallly went with MariaDB as a database
    * [PHPmyAdmin GUI](phpmyadmin.md)

