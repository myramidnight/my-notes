# PostgreSQL on your VPS
It's steadily gaining popularity and is very useful, so why not use it?

## Installing PostgreSQL
> The guide I used: [Installing PostgreSQL in Ubuntu 20.04](https://www.tecmint.com/install-postgresql-and-pgadmin-in-ubuntu/) 

> The later half of the tutorial explains how to get PgAdmin4 working (it's like phpMyAdmin for postgreSQL)

```
apt update
apt install postgresql
```


Useful commands to check if a service is active, running and enabled under the systemd. Probably works to check other services too.
```
systemctl is-active postgresql
systemctl is-enabled postgresql
systemctl status postgresql
```

Checking if PostgreSQL is ready to accept connections from clients
```
pg_isready
```
## Using the PostgreSQL through terminal
### Opening the `psql` as admin user `postgres`
1. Swap over to the `postgres` user
    ```
    su - postgres
    ```
1. Go into `psql`
    ```
    psql
    ```
    > You can exit it by pressing `ctrl+D`
### Creating users
1. Create a new user in database
    ```
    CREATE USER myuser WITH PASSWORD 'securePW';
    ```
2. Create the database
    ```
    CREATE DATABASE mydatabase
    ```
3. Creating a superuser
    ```
    GRANT ALL PRIVILEGES ON DATABASE mydatabase to myuser
    ```
#### Checking the users
You can actually check if users have passwords (sometimes you have to be sure). The following statement will get the list of encrypted passwords for each user.
```
select * from pg_shadow;
```
And to change user passwords
```
ALTER USER user_name WITH PASSWORD 'new_password';
```
## Create a database
### Using `Schemas` (Table collections)
> [PostgreSQL docs: Schemas](https://www.postgresql.org/docs/8.1/ddl-schemas.html)

Two different schemas can have objects by the same name and things don't get mixed up. This includes tables, indexes, sequences, various data types, functions and operators.

The `public` schema is the default one, and any table that is created without specifying the schema name will be created within the public schema.

Schemas refine the structure within a database. Tables in different schemas can be joined, just have to specify the schema name before the table. `myschema.mytable` or `mydb.myschema.mytable` to be very specific (or if in different database on same server).

```
CREATE SCHEMA myschema;
```

## Useful commands in the active `psql` terminal

* List all schemas
    ```
    \df
    ```
* List all available databases
    ```
    \l
    ```
* Switching between databases
    ```
    \c database_name
    ```
* List available tables in current database
    ```
    \dt
    ```
* Getting details of particular table
    ```
    \d table_name
    ```
* Check current version of PostgreSQL
    ```
    SELECT version();
    ```
* Seeing previously executed command 
    ```
    \g
    ```
* Help (just press `q` to quit the help info )
    ```
    \?
    ```
* Getting info about syntax of any PostgreSQL statement. Links to further documentation.
    ```
    \h DROP TABLE
    ```
* Knowing the execution times of queries (it will print after query is done)
    ```
    \timing
    ```
* Open the last executed command in text editor (lets you edit and rerun it)
    ```
    \e
    ```

## Accessing the database through PgAdmin4 on Apache server
Just navigate in browser to `<your_site_ip>/pgadmin4` to access the interface once the web setup has been set up.