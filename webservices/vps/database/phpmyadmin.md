# PhpMyAdmin (PMA)
For accessing your MySQL/MariaDB database with GUI. 

After I installed it, I had to manually include the phpmyadmin in the `apache2.conf` file in order for it to load, by adding the following line. Of course you should try to see if it loads before messing with the config file.

```
Include /etc/phpmyadmin/apache.conf
```
* https://websiteforstudents.com/install-phpmyadmin-on-ubuntu-20-04-18-04-with-apache/

* https://help.ubuntu.com/community/phpMyAdmin


## Upload/Save on server
If you want to be able to upload and save your exports/imports on the server, then you can simply create the directories and then tell _phpMyAdmin_ where to find them in the `etc/phpmyadmin/config.inc.php` file
```php
$cfg['UploadDir'] = '/path/to/upload/dir';
$cfg['SaveDir'] = '/path/to/save/dir';
```
After you have set that up and saved the config file, then you just have to relog into the PMA, and the options to use those save/upload directories will be available in the export/import. Perhaps a restart of apache server is needed for good measure.

## Upload file size limits
You will most likely need to increase the default limits at some point. Should simply have to locate `php.ini` file that your installation is using for reference. In a apache server it will most likely be `etc/php/version/apache2/`.

The following options are the ones you are looking for:
```php
post_max_size = 30M
upload_max_filesize = 30M
memory_limit = 128M
```
> Just remember that `upload_max_filesize` needs to be equal or smaller than `post_max_size`. Then restart the apache server.

### Issues (solved with `php-fpm`)
Check out `php-fpm` if you happen to have issues as I did with changing these options. Anything I changed didn't seem to register, even if the `phpinfo()` confirmed that I was indeed editing the correct file.

It is supposed to be a good option for managing multiple versions of php for different users (sometimes you just need a specific version for your application/website after all).

Once the `php-fpm` is installed and connected to your apache server, then you can edit the `php.ini` just as before, just make sure to be editing the correct one (most likely located within `etc/php/version/fpm/` this time)

Lastly restart both apache server and php-fpm for all changes to apply properly.

## Script Timeout
Well, there are of course limits in place so a script doesn't run endlessly. But sometimes the timeout is simply too short.

Look for the following line in the `php.ini` to fix that.
```php
max_execution_time = 30
```
and then edit/add the following to the `config.inc.php` in your `etc/phpMyAdmin`
```php
$cfg['ExecTimeLimit'] = 0;
```
### Timeout in apache config
If that doesnt work, then check out the `etc/apache2/apache2.conf` to increase the timeout limit, if that was the cause of the timeouts.
```conf
#
# Timeout: The number of seconds before receives and sends time out.
#
Timeout 300
```