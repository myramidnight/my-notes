# Web development extras
It might not strictly speaking be programming, but it is relevant to developing things for the web. 

## Terms
* OOP (Opject Oriented Programming)
    > 
* PWA (Progressive Web Apps) 
    > A website that delivers an app-like experience, combining the best of the web and native apps.
* SSR (Server Side Rendering)
    >Websites that are generated on the server and then sent to client as fully rendered HTML
* CSS (Cascading Style Sheet)
    >It is what makes a website pretty
* SEO (Search Engine Optimization)
    >Improve a website's visibility to search engines.

## Animating
Animations are very important for a user experience, it can give them feedback that things are working, that something is happening, or simply makes things delightful to interact with.

A button that changes color on hover, or transforms after clicking, those are animations.

* [CSS animations](https://www.w3schools.com/css/css3_animations.asp)
    > Keyframes and animation properties in CSS let you animate targeted elements for smooth transitions and feedback.
* SVG (_Scalable Vector Graphics_)
    > SVG are vector images, crisp images even if you scale their size. And since they are just shapes, you can animate them using __CSS__ or __JavaScript__.
    * [Figma](https://www.figma.com/)
        > Figma a vector based editor that is generally used for web/app design, so creating SVG images and icons with it is ideal.
    * [SVGator](https://www.svgator.com/)
        > Platform to create SVG animations that can be exported as JavaScript or CSS animations.

## Browsers
* ### Multiple Browsers
    > Always have at least one extra browser, specially the popular ones. Sometimes when developing things behave differently depending on the browser and just using a different one can solve things. This has become less of an issue in modern browsers, but it still happens occasionally.
* ### Styling
    > All browsers have _different default styling_, so it is quite important to set all padding and margin to `0` at the beginning of your stylesheet to remove these inconsistencies, __if you are creating a style__.
## Inspect (Dev tools) in browser
It seems most browsers are sharing the same dev tools, just with different styling in most cases. This makes the tools familiar regardless of what browser you are using.

* You can _right click_ any element on the website and choose `inspect element`, this will select/highlight it in the dev tools.
* `Ctrl + Shift + I` opens these tools as well
    >This works within webapps as well (websites that act like apps), such as _discord_ for desktop. They are actually just opening the same thing as you would in browser, just within it's own specialized window.
### Viewing the style
The inspector opens the `Inspector` tab by default, which lets you view all the elements on the website, see their classes and active style. 
>This view is useful to see how to target somem elements with style, as well as experimenting with the style of that element because you can apply styles in that tab and see it in action (just remember that it doesn't save anything). It also shows you the margin and padding of selected element

There is also the `Style editor` tab, which shows the stylesheet. It allows you to save the stylesheets as well.
* #### Viewing network traffic
   >Under the `Network` tab in these tools, you can find a checkbox to __disable cache__, because cache can be your enemy when you are developing and want to see your latest changes.
* #### Viewing the storage
    >You can view your cookies, cache and local storage of current website in the `Storage` tab. 