# Forms in react
It can be a tricky to do forms in react, specially when you want to cut into sub-components.

The main reason for the issues that migh arise is because of the way React does and renders things. The tricky part is to make it notice that anything happened, and keeping track of things between components.

## onChange handlers
We need a way to store the values of form fields throughout the process, and you might have segmented the form into sub-components where each have their own state.

Form fields have this property called `onChange` that allows you to pass a callback function to handle the _event_, and the current value of that particular field is found in the event passed to the callback: `event.target.value`

* You could use states that you can update through the callback, passing the constantly changing value. Then create a `onSubmit` handler that would prevent default behaviour and collect all those values from the state/s you stored them in througout the process.
* You could also design the `onSubmit` to call upon all the relevant field elements and extract the value from them. 

I'm not sure, but you might run into the issue of parts of the form resetting when the app would rerender, all depends on how you implement the form in React, so you could use those states I suggested earlier for storing the values through `onChange` to become the __default value__ of the field whenever it renders, keeping things consistent.

## Using `react-hook-form` 
There is this very neat package you can install to make handling forms in react so much easier, so that you don't have design your own onChange/onSubmit handlers, because it creates reference for you and stores all the constantly changing values that can then be delivered to the `onSubmit` handler.

At least trying it makes you aware how forms are expected to be handled, through references and your own `onChange`/`onSubmit` handlers.

It might seem confusing, but whenever you have a text input, then those components tend to be called __controllers__ in React.