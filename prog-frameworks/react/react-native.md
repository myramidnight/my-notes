# React native app
This library lets you create apps for android, iOS and even the web without having to focus on a specific platform. React will generate a build for each platform based on your code, which lets you focus on your app instead of the platform.

## Useful plugins/addons to React native
1. `react-native-paper` 
    > This will include google's [Material design](https://material.io/components) in your application, so you can have a nice looking app without the hassle of creating the the style from scratch.
    >
    > [Cross-platform Material Design for React Native](https://callstack.github.io/react-native-paper/index.html)
