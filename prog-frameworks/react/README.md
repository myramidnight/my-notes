# React
React is a javascript library (in browser or nodeJS) that creates user interfaces, ideal for frontend development.

There are two types of apps you can create with React
1. #### Web App
1. #### Native apps
    > React lets you focus on creating a single app, and then compiles them for both Android and iOS simultaniously if you want (can have it only compile for one or the other, but can expect the other to look and behave relatively the same). 
    >
    >This removes the complexity of trying to maintain the same application on two fronts (Android and iOS) where both platforms have very unique ways of doing things. It would be double the work to create identical products for those two platforms othervise.

### Is there a difference?
Since __react native__ deals with local hardware on mobile devices, which are unique to either platform (Android and iOS), even though you do not have to take those differences into account because the compilor will manage adjusting what you created so it works on those specific platforms.

But the biggest things that might be considered for differences is how the program manages to communicate to the hardware. __React native__ already has ready components that become your base tools, because you're not working with _HTML_ elements, you are working with things that eventually realy on the native hardware of the mobile device.

Besides for that, you would still program and structure things the same way, because it's still the same framework, all the same tricks for the components are the same. 

> Most packages for __react__ have special versions suited for __react native__, to include ready made components adjusted to the needs of native applications.

* So if you are making a __native app__, then you will need a emulator or physical phone to preview what you're making.  
    * usually the app preview would automatically refresh/reload when changes are made to the code 
* Othervise if you are doing a __web app__, then you simply require a browser to reach the localhost where the server is running
    * the page needs to be manually refreshed in order to view any changes to the code, as expected for webdevelopment


## Additional information 
### Types of components
They essentially work the same, slight difference in how they pass props around (different syntax so to speak). 
* __Class component__
    * lets us define a local/component state
* __Function component__
    * lets us the magical __hooks__

## React Index!
* ### [React Web App](react-init-web.md)
    > Will render your app in a browser. It has become quite popular to create web-apps that behave similar to native apps (having some styling for smaller screens and such to complete the experience). Then you don't have to maintain multiple fronts for your service (website, Android and iOS), just the single web-app.
    >* Web-apps have grown quite flexible, and can send requests for access to hardware, just like a native-app would ("can I use your camera?" and such)
    >* You can even save web-apps on mobile, where it appears along the other app icons. It's technically a link specifically to that website (even opens in a stand-alone window, instead of your prefered browser, completes the app experience).
    >* web-apps can even be implemented to be able to function a bit without internet, same as apps (cookie magic, local storage and cache), instead of having to show nothing on the screen.
* ### [React Native](react-native.md)
    > React lets you focus on creating a single app, and then compiles them for both Android and iOS simultaniously if you want (can have it only compile for one or the other, but can expect the other to look and behave relatively the same). 
    >
    >This removes the complexity of trying to maintain the same application on two fronts (Android and iOS) where both platforms have very unique ways of doing things. It would be double the work to create identical products for those two platforms othervise.
* ### Features and tips
    * [__Rendering__](react-rendering.md)
        > html representation of javascript
    * [__Components__](react-components.md)
        >* Function or Class components
        >* how to structure your components
    * [__Props__](react-props.md)
        > Props is how you pass information between components, like the properties of HTML elements that you can define.
    * [__Hooks__](react-hooks.md)
        > Brings the many features that were exclusive to class components into effect for function components.

    * [Input](react-input.md)
    * [React redux](react-redux.md)
        > Give react a global state
    * [React eslint](eslint-react.md)
        > keep the code consistant, specially durint teamwork.
    * [Best practices](react-best-practice.md)
        > PropTypes, CHildren props, Context Provider, Error Boundary, JSX fragments