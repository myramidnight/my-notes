# Props
This is how you pass arguments to a Component.


## PropTypes
It is very nice to be able to define the type of data being used, then you do not have to write alot of error catches for all possible type errors.

```js
import React from "react";
import PropTypes from "prop-types";
import { View, Text } from "react-native";

// arguments passed through props
const PropsComponent = ({ name, age, calculate }) => {
    return (
        <View>
            <Text>Name: { name }</Text>
            <Text>Age: { age }</Text>
            <Text>Calculation: { calculate() }</Text>
        </View>
    )
}

// define the proptypes before export
PropsComponent.propTypes = {
    name: PropTypes.string.isRequired,
    age: PropTypes.number,
    calculate: PropTypes.func.isRequired
}

export default PropsComponent;
```
