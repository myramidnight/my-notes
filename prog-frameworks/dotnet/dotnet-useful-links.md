
## Working with JSON
* [Working with JSON in `.NET` core](https://codeburst.io/working-with-json-in-net-core-3-2fd1236126c1)
* [Serialize/Deserialize JSON](https://docs.microsoft.com/en-us/dotnet/standard/serialization/system-text-json-how-to)

## External API requests
* [Consuming external API](https://www.yogihosting.com/aspnet-core-consume-api/#httpclient)
* [Creating HTTP requests (video)](https://www.youtube.com/watch?v=k-5_xI3W0dk)

## Automapper
[Automapper for your Dtos](https://docs.automapper.org/en/stable/)