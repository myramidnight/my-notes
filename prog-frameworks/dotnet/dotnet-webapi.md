# Dotnet WebAPI
These notes are specific to the WebApi project template that is provided by `dotnet`. For the sake of reference, these notes are done using the `3.1` dotnet core.

### Layered architecture
This is of course optional, but it is a good tactic to keep an organized application. Each project/layer would only talk to the interfaces of another project/layer. It limits the contact surface and narrows down causes of errors.

We would have one application, which would contain multiple dotnet WebApi projects, one for each layer or purpose generally, such as `Models`, `Repositories`, `Services` and one to act as the interface, in this example it has been named `API`.

They could technically be kept as multiple folders within a single project, but for a layered architecture it is good to have them in separate projects. We connect the projects by using `reference`.

## Controllers (Routers)
There will be a demo controller file in your project which you can just delete (or use as reference). You set all the possible routes within these controllers, say if they are get/post/put and what route is listening to these requests.

The controller will forward the requests to the right methods within the application.

Here we have the `Cryptocop.Software.API` application, and witin the `API` project we have the following controller.  
```c#
using Microsoft.AspNetCore.Mvc;
using Cryptocop.Software.API.Services.Interfaces;
namespace Cryptocop.Software.API.Controllers
{
    //main route
    [Route("api/exchanges")]
    [ApiController]
    public class ExchangeController : ControllerBase
    {
        //Internal connection to an Interface 
        private readonly IExchangeService _exchangeService;

        public ExchangeController(IExchangeService exchangeService)
        {
            _exchangeService = exchangeService;
        }

        [HttpGet]   //Http handler for a GET request
        [Route("")] 
        //forwards the request to specific method in Service layer
        public IActionResult GetExchanges() => Ok(_exchangeService.GetExchanges());
    }
}
```
> It helps to organize the code to have each base route as it's own controller. Each controller would interact with specific Service interfaces 

## Models
```c#
using System.ComponentModel.DataAnnotations;
namespace Cryptocop.Software.API.Models.InputModels
{
    public class LoginInputModel
    {
        [Required]
        public string Email { get; set; }

        [Required]
        [MinLength(8)]
        public string Password { get; set; }
    }
}

```
You will create projects with the `webapi` template

### Register your services
If you get an error that the application could not resolve a service, it most likely means you forgot to register your services under `ConfigureServices` in the `startup.cs`.