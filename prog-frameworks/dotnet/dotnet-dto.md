# Data Transfer Objects (Dto)
These are used to move data around the application. We create classes that will be filled with the expected data and transfer them between classes/layers. 

These are also called models.

## Setting Nullable values
There can come up issues related to `null` being fed to a Dto, which makes it very nice that you can assign default values.
* [Nullable reference types](https://docs.microsoft.com/en-us/ef/core/miscellaneous/nullable-reference-types#non-nullable-properties-and-initialization)
* [stackOverflow: Nullable reference](https://stackoverflow.com/questions/59118282/nullable-reference-type-in-c8-when-using-dto-classes-with-an-orm)

```C#
    public string ServiceUrl { get; set; } = null! ;
    //or
    public string ServiceUrl { get; set; } = default! ;
```

## Using `Newtonsoft.Json` in Dto
Sometimes you are mapping to the object from a JSON object, but it has it's own null handling. This example below shows how you can tell it if the json property has different name than the Dto property, as well as ignore null as a value (instead of throwing errors)
```C#
[JsonProperty("price_usd", NullValueHandling = NullValueHandling.Ignore)]
```
> [Fix null value on json data](https://social.msdn.microsoft.com/Forums/vstudio/en-US/f3bdb932-9d9e-4253-a664-903a742e77b2/u81-how-to-fix-null-value-on-json-data?forum=wpdevelop)