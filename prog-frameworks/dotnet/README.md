# .NET
It's a framework for applications made by Microsoft, written in `c#`.

## Install the dotNet core
1. [Get the .Net core](https://docs.microsoft.com/en-us/dotnet/)
2. Get a code editor if you don't have one already. Visual Studio Code comes recomended.
3. Run the `dotnet` command in terminal to see if it works. It has some inbuilt documentation.

### Extensions for VSCode
It is very useful to have extensions such as `jchannon.csharpextensions` or `ms-dotnettools.csharp` to get the syntax and easily create new C# class files and debug the code. VSCode cannot do debugging without appropriate extensions for the programming languages.

## Creating a project from templates
First you will of course need to create a directory to house the projects, such as `MyProject`
1. Type `dotnet new` into the CLI to get a list of ready templates you have available for your project.
3. You can get additional help with `dotnet new --help`
    > Naming conventions of modules within the project is `MyProject.ModuleName`
4. You can create new project in at least two ways. 
    1. Manually create the subdir following the naming conventions, and then run following command in the subdir to populate it with files. 
    >```
    >dotnet new <template>
    >``` 
    2. Or you can have the `dotnet` create the directory for you with a specified name.
    >```
    >dotnet new <template> --name ModuleName
    >```


## [Creating a WebAPI project](dotnet/dotnet-webapi.md)
