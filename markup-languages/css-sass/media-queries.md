# CSS: Media Queries


They can be included in the HTML with conditions (`link media=""` ) or directly in the stylesheet.

```css
@media [media] and [expression] and [orientation] {
    ...styles
}
```
* __media__: screen, print, speech, all
    > How is the content being viewed? Is it for a screen or print layout?
* __expression__: min-width, max-width
    > window size constraints, when should the contained style be used?
* __orientation__: landscape, portrait
    > notice the orientation on mobile devices

## Media features (expressions)
These features are optional, but they let you style things based on the device.
```css
@media print {
    /* style for print layout*/
}

@media not print {
    /* style for everything except print */
}

@media screen {
    /* style for displaying on a screen */
}

@media all {
    /* style applies to all conditions */
}
```
> `all` is kind of pointless as a media-query, because any code outside of `@media` will also apply to all things in the same manner as using this expression would.