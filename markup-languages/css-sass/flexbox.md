# Flexbox
You can probably find all reference you need using the [guide to flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) by CSS tricks (they even sell it as a poster, it amuses me).

## Flex examples

![Column wrap](examples/column-wrap.png)
```css
.container {
    display:flex;
    flex-flow: column;
    flex-wrap:wrap;
}
.container div{
    flex:50%
}
.container div:nth-child(1), 
.container div:nth-child(6){
    flex:100%
}
```

![Column align](examples/column-align.png)
```css
.container {
    display:flex;
    flex-flow:column-reverse
}
.container div:nth-child(1),
.container div:nth-child(5) {
    flex-grow:1;
    width:50%;
}
.container div:nth-child(5) {
    align-self:flex-end;
}
```