# PlantUML for diagrams
This is a specially designed markup language to design UML diagrams. It generates images from the code in a similar manner as LaTeX.

* [PlantUML.com](https://plantuml.com/)  
* [Hitchhiker's guide to PlantUML](https://crashedmind.github.io/PlantUMLHitchhikersGuide/)
* [Beautiful quick diagrams (video introduction)](https://www.youtube.com/watch?v=EM-cvRubP4g)

## Editors
I personally just installed a extension in VisualStudioCode for PlantUML. But regardless of what you use to create the code, you will need `graphviz` installed to generate previews locally. 

Whatever you pick to code in, it will probably have documentation on how to set up.

* [Jebbs's PlantUML extension for VSC](https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml)
* [keisuke's plantUML online editor](https://plantuml-editor.kkeisuke.com/)

### file extensions
Oddly enough, plantuml has multiple supported file extentions.
```
    *.wsd
    *.pu
    *.puml
    *.plantuml
    *.iuml
``` 

## Index
* ### [Styling PlantUML](plantuml-style.md)