# Styling PlantUML diagrams
Everyone seem to agree that the default rendering of the plantUML diagrams are quite outdated and unappealing, but did you know that you can customize the rendering? 

### Handwritten look
Here is a simple snippet to create a handrawn look for your diagrams.
```
skinparam handwritten true
skinparam monochrome true
skinparam packageStyle rect
skinparam defaultFontName Buxton Sketch
skinparam shadowing false
```

### Make PlantUML Pretty with Styles
You might find the default renderings of your PlantUML code to be ugly, most do, and that is why you might love to hear that you can import themes! 

Here are examples of themes for plantUML, which you might have found already if you watched the video I linked at the top.

* [RedDress plantuml themes](https://github.com/Drakemor/RedDress-PlantUML)
