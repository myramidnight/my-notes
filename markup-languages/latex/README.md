[back to index](./README.md)
# Just some notes about the _LaTeX_ text editor
> __LaTeX__ is a document preparation system. When writing, the writer uses plain text as opposed to the formatted text found in WYSIWYG (_"what you see is what you get_") word processors like _Microsoft Word_. 
>* [wikipedia](https://en.wikipedia.org/wiki/LaTeX)

## LaTeX editors
* [Overleaf](https://www.overleaf.com/) is a browser-based _LaTeX_  text editor, requires no install.
* [TeXMaker](https://www.xm1math.net/texmaker/) is a very popular _LaTeX_ editor program. I reccomend it.
* [Visual Studio Code (extension: LaTeX Workshop)](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop) is very appealing.
* There exist many many more editors, these are not exclusive.

> There are always pros and cons with using a browser-based tool like [Overleaf](https://www.overleaf.com/), so it does not really matter what editor you choose as long as it is based on _LaTeX_. I just personally don't like having to rely so heavily on my internet connection being stable in order to edit my files.

The code is then rendered as images or _PDF_ files, depending on the editor.  

## Index
* ### [TexMaker (editor)](latex-texmaker.md)
  >It is my editor of choice, I like having things on my computer rather using overleaf. Though overleaf has great documentation for LaTeX in general
* ### [Basics (and handy tips)](latex-basic.md)
  >Just things I've picked up on
* ### [Importing](latex-import.md)
  >Importing allows you to put sections/chapters into their own lines, to keep the main file free of clutter
* ### [Structure](latex-structure.md)
* ### [Theorems](latex-theorem.md)
  >Create your own theorems, even box them in fancy