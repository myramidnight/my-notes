
## General about LaTeX
### Using existing templates
People have created alot of ready-to-use templates for others to use, many can be found on __overleaf__ for example, and since all _LaTeX_ editors are built around the same code engine, it is possible to transfer code between them. The easiest way to learn to code is to view examples that can be found in such templates.

> It is pretty nice to just use the _overleaf_ documentation on how to use _LaTeX_

### Packages
To use __packages__ in _LaTeX_ allows you to __import__ extra features, similar to _Python_ and other programming languages. You add them to the top of the file with `\usepackage{package_name}` or `\usepackage[feature_name]{package_name}`. 

When packages are included, it might ask you to approve of fetching the package if it is not already on the computer. 

A few handy packages that I've had to use.
* `\usepackage{amssymb}` lets you use certain set symbols
* `\usepackage{tikz}` for drawing diagrams
* `\usepackage{pgfplots}` for drawing graphs
* `\usepackage{amsmath}` for creating matrices
* `\usepackage[utf8]{inputenc}` lets me use icelandic characters

### Writing LaTeX code
__LaTeX__ coding can be compared to _HTML_ when it comes to tags, while _HTML_ uses `<` and `>` indicate tags, _LaTeX_ uses `\begin{}` and `\end{}` for indicating specific tag types and the type is named within the curly braces `{}`.

All code begins with a backslash `\`, such as `\newpage` to tell the program to create a new page when rendering the _PDF_.
#### Inline equations
These dollar-sign curly braces are required to write inline equations: `{$ $}`
#### Multiline equations
Similar to the inline equation tags, the multiline version is `{$$ $$}`. Some equations appear differently depending on if you placed the code within inline or multiline tags (sums for example).

### Beginning the document
First you need to add the packages and whatnot you wish to use. I like to use the following snippet which allows me to write icelandic letters and makes some adjustments to the default PDF rendering.
```tex
\documentclass[12pt,a4paper]{article}
\usepackage[margin=0.75in]{geometry}
\usepackage[T1]{fontenc} % Support Icelandic Characters
\usepackage[utf8]{inputenc} % Support Icelandic Characters
\usepackage[shortlabels]{enumitem} % customizable lists
\usepackage{mathptmx} % Use Times Font
```
And then all content will be placed within the body of the document
```tex
\begin{document}
  content
\end{document}
```
## Handy things to know
### Enumerated lists
The __shortlabels__ package is very handy (but optional) for allowing for simple customization of list items, such as specifying what the list marker should look like. 
```tex
\usepackage[shortlabels]{enumitem}
%default enumerated list will just created a numbered list
\begin{enumerate}
  \item %item 1
  \item %item 2
  \item %item 3
\end{enumerate}

%using the 'shortlabels' to easily create a alphabetical list.
\begin{enumerate}[a)]
  \item %item a)
  \item %item b)
  \item %item c)
\end{enumerate}

\begin{enumerate}[ ] %empty shortlabel removes the list-style.
  \item %item with no list-style
  \item[k)] %item k)
  \item %item with no list-style
\end{enumerate}
```
