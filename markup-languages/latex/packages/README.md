# Packages that I've used
When we're composing latex documents, we add all the 3rd party packages we want at the top with `usepackage` statements. They add extra functionality, ways to render the content.

We only want to add them when we need them, because they take time to load, and when you render a latex document with any packages you might not have used before, you could be asked to confirm if you want to install that package (always need to be careful of 3rd party content).

The lists below just count the ones I've actually used, and some notes about how I used them.

>Just google their names to find user manuals for the packages or lists of commands.

Some of these will require other packages to be included as well

## General purpose
* `import`
    > To import `tex` files, allows you to organize your document into multiple files
* `graphicx` 
    > Allows you to include images in your documents
* `inputenc`
    >Lets you accept different input encodings
    >```latex
    >\usepackage[utf8]{inputenc} % Support Icelandic Characters
    >```
* `mathptmx`
    >This package defines Adobe Times Roman (or equivalent) as default text font, and provides maths support using glyphs from the Symbol, Chancery and Computer Modern fonts together with letters, etc., from Times Roman. It supersedes both the original times and the mathptm packages.
* `hyperref`
    >Support for hyperlinks (clickable links in the digital document). This is very handy for the table of contents as well, makes it into links to jump to desired sections. It supports styling as well
    >```latex
    >\hypersetup{
    >    colorlinks,
    >    citecolor=black,
    >    filecolor=black,
    >    linkcolor=black,
    >    urlcolor=blue
    >}
    >```
* `enumitem`
    >This package provides user control over the layout of the three basic list environments: enumerate, itemize and description
    >``` latex
    >\usepackage[shortlabels]{enumitem} % shorthand labels for list items
    >```

## Theorem boxes
Since we can use theorem to create various custom boxes to frame content, this can have it's own section
* `tcolorbox`
    >Styled and colored theorem boxes!
## Styling
* `mdframed`
    >Lets you frame content (not just theorems)
## Math packages
* `amssymb`
    > Adds many math symbols to your collection, you use them with backslash commands, or when they are providing characters, the got the following commands (just specify what character you want within the braces)
    >```latex
    >\mathbb{N}     % Symbol for Natural numbers
    >\mathfrak{a}   % Gives you fancy caligraphy characters, here an 'a'
    >```
* `amsmath`
    >LaTeX and math, it's what we all use it for.
* `tikz`
    > Is the base for many other packges that generate svg graphs, diagrams or images. It has many sub libraries.
* `cancel`
    >Lets you slash out things in math (when things cancel out)
* `logicproof`
    >For writing logic proofs in natural deduction