
## Custom theorems
You can create custom theorems for whatever you need. 

```
\newtheorem{definition}{Definition}
or
\newtheorem{definition}{Definition}[section] %to numerate it based on section numbers
```
>Then you can use it in your document. The first is what it'll be called as, the later field tells it what to print as the type of theorem.
> 
>`\begin{definition}[Name of Definition]` would print out: 
>> __Definition 1 (Name of Definition)__ _blablah content of definition_



### Fancy Theorem boxes
* [Box around theorem (stackexchange)](https://tex.stackexchange.com/questions/36278/box-around-theorem-statement)

This creates a purple box around the theorem
```latex
\usepackage{tcolorbox} %making fancy theorem boxes
\tcbuselibrary{theorems} %lets you use the newtcbtheorem

\newtcbtheorem{myformula}{Formula }{%
	theorem name,% this removes the numbers in the theorem
	colback=violet!5,% background
	colframe=violet!35!black!70,% frame
	fonttitle=\bfseries, %
	width=0.9\textwidth% width of box
}{th}
```

```latex
\begin{myformula}{Classical probability}{} 
Hey

\end{myformula}
```
> The empty field is an extra argument, if you skip it, then the theorem box will eat the first character of the content, as odd as that might seem.
>* [first letter in body disappears](https://tex.stackexchange.com/questions/328622/using-tcolorbox-the-first-letter-in-the-body-disappears-what-can-be-done)
### Remove the numbers from the theorem
```latex
\usepackage{ntheorem}
\theoremstyle{nonumberplain} %Removes the numbers from the theorem
\newtcbtheorem{myformula}{Formula }{%
	theorem name,% this removes the numbers in the theorem
	colback=violet!5,% background
	colframe=violet!35!black!70,% frame
	fonttitle=\bfseries, %
	width=0.9\textwidth% width of box
}{th}
```

### Plain looking frames
```latex
\documentclass{article}
\usepackage{mdframed}
\usepackage{lipsum}

\newmdtheoremenv{theo}{Theorem}

\begin{document}

\begin{theo}[Theory Name]
\lipsum*[1]
\end{theo}

\end{document}
```

### Interesting package for making boxes
* [Tcolorbox manual (pdf)](http://ctan.mirrors.hoobly.com/macros/latex/contrib/tcolorbox/tcolorbox.pdf)
    >This might become my new favorite thing for styling things in latex, heh

It uses `tcbuselibrary{theorem}` to load various libraries that make use of _the color box_ package (the one used in above examples)