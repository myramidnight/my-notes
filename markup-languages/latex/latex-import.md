# LaTeX input/import
I found it quite handy, if not essential, to be able to break up a large LaTeX document into managable pieces by topic or chapters. This also allowed me to create PDF files of only specific chapters if I wanted to.

## Creating a file that can be included
You create a sub `tex` file for the specific purpose of being included in other files, their content being code that would normally be placed within the `\begin{document}`, so it lacks all the document initation. 

You create a main `tex` file that you then import all the sub files into, and render that in order to see what you've been writing. 
## Import and Input
### Input from same directory
You can include another tex file by using `input` that links to the specified file and simply includes all it's content. 
```tex
\input{khr_4-1.tex}
\input{khr_4-3.tex}
```

### Import from other folders
And when getting files from sub folders. Here the folder is named `khr1`
```tex
\import{khr1/}{khr_1-1.tex}
```

### Dependencies
When you are using anything from packages in your files, then you of course need to make sure that the importing file has them included, since they are not defined within the sub files.

## Examples
Trimmed them to only show the `\begin{document}` parts.
### Input example 
Where I rendered single chapters, with comments to remind me what I was including.
```tex
%------------------------------------------------------------------
% DOCUMENT START HERE
%------------------------------------------------------------------
\begin{document}
    \maketitle
    %contents
    \tableofcontents
    \newpage
    \input{khr_1-1.tex}
    \newpage
    %KHR 1.3 : Jafngildar yrðingar ('propositional equivalances')
    \input{khr_1-3.tex}
    \newpage
    %KHR 1.4 : Umsagnarökfræði ('predicate logic')===========
    \input{khr_1-4.tex}
    \newpage
    %KHR 1.7 : Sönnunaraðferðir ('introduction to proofs')===
    \input{khr_1-7.tex}
\end{document}
```

### Import example
Where I rendered the whole book
```tex
%------------------------------------------------------------------
% DOCUMENT START HERE
%------------------------------------------------------------------
\begin{document}
    \maketitle
    %contents
    \tableofcontents
    %KHR 1.1 : Yrðingarrökfræði =============================
    \newpage
    \import{khr1/}{khr_1-1.tex}
    %KHR 1.3 : Jafngildar yrðingar ('propositional equivalances')
    \import{khr1/}{khr_1-3.tex}
    %KHR 1.4 : Umsagnarökfræði ('predicate logic')===========
    \newpage
    \import{khr1/}{khr_1-4.tex}
    %KHR 1.7 : Sönnunaraðferðir ('introduction to proofs')===
    \newpage
    \import{khr1/}{khr_1-7.tex}
\end{document}
```