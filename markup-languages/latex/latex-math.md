# Handy math related latex contexts
### Horizontal curly brackets
```tex
%Curly under
`\underbrace{}_\text{}`
%Curly over
`\overbrace{}^\text{}`
```
### Writing matrices 
They work just like _tabular_ lists, where each column is seperated by a `&` and create new rows with `\\`, though it requires you to add a `\[` and `\]` around the matrices. You can have multple matrices or text within the same pair of tags, it will just arrange everything on the same line.
```tex
\usepackage{amsmath}
\[
  \begin{bmatrix}
    1 & 0 \\ 
    0 & 1
  \end{bmatrix}
\]
%creates a square-braced matrix
```