# Foundations of a good wiki
A good foundation for a wiki is essential for the longetrm, though it depends a bit on the size of the wiki and the community that contributes to it. A well structured wiki can grow and be maintained easily.

## Look at other wikis
Any wiki that uses `mediawiki` for a base can hold examples of how to make a good wiki. See something good? Look into what they did. Even though they might be using extentions that you don't have, you can always gleam something from it and even reverse engineer it to fit your needs.

## Be aware of the different namespaces
Mediawiki uses `namespaces` to seperate different types of pages/content, and some namespaces even have special powers. Most of them (except `main`) can even have sub pages.
* `Main` space is where your main content lives, the wiki itself. No prefix.
* `Templates` can be included on any other pages (in any namespace), their content will appear when the template is used.
* `Category` lets you group pages together, this creates a easy link between them to view all other pages that share the specified category.
* `Help` space is like a inside-wiki just for the contributors. 
* `Special` is reserved for the wiki core features, alot of good tools in the `special pages` that make maintaining the wiki even easier.
* `File` space is where images and files are uploaded. Might need extensions if you want to support more file types than basic images.
* `User` space is probably a bit underrated, a perfect place to create a sandbox for the user to experiment in. 
    > The user's profile page is possibly the easiest page for them to reach when logged in (their username link in the top corner). This can also be a profile of course, and best of all, this space supports sub pages. These pages can edited by anyone that can contribute to the wiki generally.
* `MediaWiki` space is part of the core, you might poke at it to change how the wiki looks, customize the sidebar.
* `Talk` pages are created via the `discussion` link on any page within the wiki, designed to allow contributors to muse over the page content, make comments about anything regarding that page. 
    > This space has a unique feature: a `Add topic` tab/button that allows users to easily add topics without having to edit the whole page. Then you can just edit the relevant section/topic to contribute to the discussion.
* `Custom` spaces can be created on the wiki, though it requires modifying the `LocalSettings.php` to include them. MediaWiki can guide you.

## Use templates
They make your life so much easier. 
* Anything that might be used in more than one place should totally be in a template.
* Tables that have grown large would benefit from being in a template.
    > it clears some clutter from the actual page, and allows you to add documentation specially about how contributors can edit that table (which isn't really possible within the main pages without seeming cluttered).
* Templates can actually take arguments, and if you have `ParserFunctions` extention installed on the wiki, then you can even add conditional statments.


### Documentation template
Documentation is key to a good wiki, and especially handy for explaining to contributors how to use the templates you've created. Wikipedia and Mediawiki do it great by making use of templates and sub-pages to avoid cluttering the actual template with all the content from the documentation. When everything is in place, then all you need to do is slap in the `{{Documentation}}` and it will automatically know where it is and create correct links to relevant pages. It's just too awesome not to copy. 

### Control what is included
The tags `<noinclude>`, `<onlyinclude>` and `<includeonly>` will be essential for good templating. Don't want the documentation for the templates to come along for the ride, or their categories either in most cases, or have included categories apply to the template itself.

## Fill out the help pages
In order to build confidence within your contributors, or even just to remind yourself of how things are done (it is easy to forget how some things work) you should have good help pages so they can help themselves. It is more fun to contribute when the guidelines are clear and the instructions easy to follow. 