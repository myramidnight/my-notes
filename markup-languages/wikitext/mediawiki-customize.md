# MediaWiki
Pages to edit to achieve various things

## Uploads
#### Customize the upload page
>```
>MediaWiki:Uploadtext
>```

#### Customize default description (the text box)
>Edit this page to change the default description on file uploads
>```
>MediaWiki:Upload-default-description
>```

## Sidebar
>```
>MediaWiki:Sidebar
>```

## Links
