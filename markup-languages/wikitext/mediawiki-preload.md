# Preloading content
You can actually create preloaded content, which the user will just have to adjust the details of. This is useful if the page will include alot of categories and templates.

1. Create a `template` that contains whatever you wish the preload to be. 
    > It is suggested to create this as a subpage `/Preload` under existing template. 
3. You can then create a link that precents the contributor with a new page or section already filled with the essential content.  


## URL arguments with special powers

### New section
```
&action=edit&section=new
```
Edits existing page and adds a new section. Contributor can fill in the title field and content. Adds the new section to the bottom of the page. Handy for long pages such as change logs. This is a feature that the talk pages employ with adding new topic.

### New page
```
&action=edit&page=new
```

### Adding preload
Add this on the create new page or section, it will automatically be preloaded with the contents of the preload template.
```
&preload=Template:NameOfPreloadTemplate
```


