# Documentation of pages
Documentation is important, specially to explain to others how to use your templates, how to contribute/edit specific pages, and even to include general information.

This is a basic documentation template setup that works real well, which was derived from wikipedia. It is quite useful to be able to view anything on other wikis, to see how they manage their content. 

### ParserFunctions
This setup relies on the `parserFunctions` extension being enabled (which is included by default, but the wiki never assumes that you'll use anything, so needs to be manually enabled). 

This extension lets you use many useful expressions, such as if-statements and switch cases. You should look into how those work before trying to change any such expression in existing templates.

## The `documentation` template
The core function of this template is to just paste in whatever was written in the `/doc` subpage of current page. This amasingly prevents any unrelated clutter on the current page that could also confuse whoever might want to edit the template with the question "where does the tepmlate end and the documentation start?" because all they will find is a `{{Documentation}}` tag, but not the contents of the documentation.

``` 
<onlyinclude><includeonly><div style="
	margin: 1em 0 1em 0;
	background-color: #fff7e4;
	border: 1px #bfa3af solid;
	border-collapse: collapse;
	color: black;
	padding: 1em;">
<span style="{{#ifeq: {{SUBJECTSPACE}} | {{ns:Template}}
    | font-weight: bold; font-size: 125%
    | font-size: 150%
    }}">{{#switch: {{{heading|¬}}}
  | ¬ =   
    <!--"heading" not defined in this or previous level-->
    {{#switch: {{SUBJECTSPACE}} 
    | {{ns:Template}} = Template documentation <hr>
    | {{ns:File}} = Summary <hr>
    | #default = Documentation <hr>
    }}
  | #default = 
    <!--"heading" has data or is empty but defined-->
    {{{heading|}}}
  }}</span>
<!---

--->{{{{PAGENAME}}/doc}}
</div>
<div style="
	margin: 1em 0 1em 0;
	background-color: #f7e1a9;
	border: 1px #bfa3af solid;
	border-collapse: collapse;
	color: black;
	padding: 1em;">
:The above documentation is transcluded from [[{{FULLPAGENAME}}/doc]]. 
:Editors can experiment in this template's [[{{FULLPAGENAME}}/sandbox|sandbox]]
:Please add categories to the [[{{FULLPAGENAME}}/doc|'''/doc''' subpage ]] within {{tag|includeonly}} tags.
</div></includeonly></onlyinclude>
```
### _Features_ of this template
* It notices in what __namespace__ the template is being used, by using the _magic word_ `{{SUBJECTSPACE}}`, and switches it's headings accordingly.
    * When used in the `Template` namespace, it will have a "_Template documentation_" heading and standardized information at the bottom about how to edit the documentation.
    * In the `File` namespace, it will switch the heading to say "_Summary_" and hide the bottom information panel.
* It provides a optional `heading=` parameter that overwrites the heading completely.

### Custom templates being used
There is a `{{tag}}` template which simply creates a neat quick exapmle of tags, with included `nowiki` and `code` tags wrapping it to display the codes instead of triggering it. Such a template is handy because writing those are tedious.
>  `{{tag|example}}` would transform into `<example>...</example>`