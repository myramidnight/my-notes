# Accessability on the web
Considering how available the internet has become, it should also be accessable to everyone. If you can design a website that is both pretty and accessable then you earn yourself extra points. 

You always want to consider accessability, because it makes no sense to exclude anyone from reaching your content, it's like loosing possible clients because you don't accomidate wheelchair access to your café.

There are many disabilities that effect people's ability to browse websites
* Bad vision or blindness, requiring a screen reader.
    * I am pretty sure braille displays would be a extention of a screen-reader, rendering things so that blind+deaf people could read the content.
* Mobility issues, being unable to navigate a mouse to click small elements.
    * Old age introduces issues with fine motor skills.
    * Some might use dictation, speaking the commands to interact with programs/websites.

We want to make the internet accessable to everyone, and that includes people that have disabilities that does not let them see the screen or cannot use a mouse. The simplest thing you can test is using the `tab` button, which is used to quickly jump between links/clickable elements.


### Improved accessability with HTML5
>It is worth mentioning that with the arrival of these new tags, it has become easier to create content that is very accessable without having to go the extra mile. Just paying attention to use proper tags when wrapping the content will paint a clear picture for __screen readers__ about what they are looking at.
>
>We might take it for granted the ability to _scan_ content to quickly find what we're looking for, glancing over headlines rather than reading the full articles or trudging through every single image description of the side content. 
>
>If all content is well defined by using the correct tags, then these tools can better navigate the content and it makes the experience of browsing much more enjoyable for those who require those tools. It is nice to be able to skip content when needed when the 