# Events in Browsers

* Event type
    > input, click, hover, focus... describes what happened 
* Event target
    > what was the target of the event, such as the DOM element
* Event object
    > object containing the details for the event. This will contain the target and type, among other details. Properties might vary based on type of event (such as mouse events having coordination). 
* Event handler
    > the function that is called when the event happens. The event object is passed to the event handler.


## Event handler
The event can be handled in muliple ways, such as within the HTML (_inline_) or through JavaScript. 
* __inline__ handling might be just as frowned at as _inline styling_ is. It makes the HTML file more complicated, and it overwrites anything.

### Invocation order of Event Handlers
* `addEventHandler`
* `addEvent`

## Event bubbling
> Multiple event handlers listening to the same event. The handler at the lowest level triggers and _bubbles_ up to higher levels. We might not want higher levels be aware of the handler triggering at lower levels.

* `stopPropagation()`
* `cancelBubble = true`
* `stopImmediatePropagation()`

## Document Events
> This would be checking if the DOM is loaded, so we are not trying to manipulate a document that hasn't been loaded yet.
* `DOMContentLoaded`

## Mouse Events
We can listen to a event triggered by the mouse device
* `click`, `dblclick`, `mousedown`, `mouseup`, `mousemove`, `mouseover`... 

### Mouse locations
>When a _mouse event_ is registered, it will contain information about the location of the mouse at the time of event, such as `offsetX`/`offsetY` based on the element, `clientX`/`clientY` being position to the window, and `pageX`/`pageY` which is relative to the page itself (even if you can't see the whole page)

## Text Events
>This follows keyboard input, such as writing into a input-element or just pressing the keyboard anywhere while looking at the page, to make keybindings and such.
* `keydown` (was the key pressed down?)
* `keyup` (was the key released?)
* `keyCode` (what key was pressed?)