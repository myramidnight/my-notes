# Hypertext Markup Language (HTML)
This is how you format text for the web, and I would not be surprised if many markup languages are just different ways to generate HTML, because many allow you to use standard HTML syntax as well (Markdown and Wikitext let you at least).

All content will be organized within the HTML markup elements, such as `<head>`, `<title>`, `<body>`, `<p>`, `<div>`... and so on.

It is important to use these elements correctly because some people use screen readers which might read things differently based on the element.

## HTML5
>Changes to HTML as a language are slow, because it is basically just a framework with limited functionality that focuses on arranging the content and tagging it. `CSS` and `JavaScript` are the languages add the complexity to websites. 
>
>HTML stayed unchanged since 1997 until 2014 with the arrival of __HTML5__, which added many new elements/tags that addressed the needs of the modern web (new tags/elements and better support for multimedia). 
>
>### New elements and specialized DIV tags 
>HTML5 made a lot of nice new tags that are specialized `<div>` elements, which previously required you to add custom defenitions if you wanted to keep screen readers in mind. Besides, it's nice to be able to escape from the "div soup" by having specially defined elements.
>
> Some of the new tags that help clear up the notorious _div-soup_ :
>> `<header>`, `<footer>`, `<main>`, `<nav>`, `<article>`, `<details>` and `<summary>`
>* [List of new elements in HTML5](http://w3schools-fa.ir/html/html5_new_elements.html)

## [The DOM (Document Object Model)](html-dom.md)
> For representing and manipulating document content on the web.

## [Accessability on the web](web-accessability.md)
> Accessability is important, even in virtual spaces.