# Markup languages
Markup languages dictates how your text will be displayed, and many might seem complex. The main use of markup would be for text documents in text editors, because most content is text based. 

__WYSWYG (What You See is What You Get)__ editors omit the step where you actually get to see the syntax, and simply show you directly how the content will look when you apply the markup. 

## [HTML](html/README.md)
The very core of how text content is displayed on the internet. HTML builds the __DOM (Document Object Model)__, wrapping the content in associated elements/tags, which in turn tells the browsers how to display the content contained within each element. All browsers have their default _style_ that they apply to these elements, which can make the content appear slightly different while staying generally the same. 

* ### [CSS](css-sass/README.md) (Cascading Style Sheet)
    >`CSS` extends the ability to style these _DOM elements_ beyond the browser defaults. Without style, all content would just appear in a single column, ordered directly by how the _DOM_ is ordered.
    >
    > `SASS/Sassy-CSS` is a framework for CSS, that make it easier to do complex styles. 
* ### JavaScript 
    >`JavaScript` gives us the ability to do programming within HTML, to manipulate these _DOM elements_ and have more complex interactions than simply clicking a link to visit the next page. 

## Markdown
For all your programming documentation needs 
> Some online git repository services might have unique/extra syntax that would only render when viewed through their website. For example, some things in the markdown on GitLab does not render properly when viewed locally or on other git services.
* ### [Markdown](./markdown.md)
* ### [LaTeX](latex/README.md)

## [PlantUML](plantuml/README.md)
