[back to index](./README.md)
# Working with `Markdown` (`.md`) files
1. They are awesome
2. They are simple!
3. You can preview them directly in Visual Studio Code by pressing `ctrl` + `shift` + `V` 

> You can actually use `HTML` elements within a markdown file.

## Markup and Markdown?
You might have heard of `markup` languages ("ívafsmál") before and might be a bit confused. 

Markup is used to define how text should be formatted. `Markdown` is a markup language that is commonly used for `README` files in `git` repositories. Examples of other markup languages includes `HTML` and `LaTeX`. 

> `Markdown` lets you use `html` formatting tags. This is handy if you already know it's syntax already and can't be bothered by learning a special markup language just to create README files.

## Basic formatting
A single break in the text allow you to organize long texts in the raw markdown, but it doesn not show up when displayed unless the text is preformatted. 

To seperate text into paragraphs, you need to leave a whole empty line between them.

## Headings
```markdown
# H1
## H2
###### H6
```

## Preformatted text/code
Using the tripple `backtick/backquote` will let you write preformatted code, and you can optionally tell it what type of code you are writing by specifying it after the opening <code>```</code> so it'll display the syntax correctly with colors. 
<pre>
``` css
  div { background: black; width:800px; }
```
</pre>


``` css
  div { background: black; width:800px; }
```

## Blockquotes
> These are handy for making small notes that stand out. Start a line with a pointy bracket `>` to create a blockquote like this one.

I have noticed that `GitLab` has it's custom multiline blockquote with a tripple pointy bracket  `>>>` at the start and end of quote. Keep in mind that they will probably not work outside of GitLab. Normal behaviour ignores them.

## Images
```
![alt text](image url)
```