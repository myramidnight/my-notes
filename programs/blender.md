Just wanted to keep this here for a bit. I have been playing a tad bit with blender, just realized I should keep notes on it! hehe

```
To copy or project Weights from one model to another,

In Object mode, Select your source model.
Shift-Select your destination model(so that it is the actively selected object),
Switch to Weight Paint mode(only works if destination model is parented to an armature).
Under the Tools tab in the toolshelf of your 3d viewport, scroll down to ‘Weight Tools’.
Click ‘Transfer Weights’
In the Operator panel below your toolshelf, under ‘Source Layers matching’, set the dropdown to ‘By name’.
Congratulations, your weights have been copied for all weighted bones in your armature.


If you only need to copy the weights of one bone onto your destination object, leave the ‘Source Layers Selection’ unchanged (Default: Active Layer)
```