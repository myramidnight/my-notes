# Jupyter and Docker
It must be the best combination ever, if your machine supports virtualization and docker.

If you have a special way you want your container to be, then you can of course create a `dockerfile`, but _jupyter_ provides official images you can use.

### Docker-compose
Here is the basic `docker-compose.yaml` contents I have been using, that starts up a minimal notebook and uses a `jupyter/` directory for storing files used within the `work/` directory inside jupyter notebook.
```yaml
services:
  jupyter:
    image: jupyter/minimal-notebook
    user: root
    ports: 
      - "8888:8888"
    container_name: jupyter_notebook
    volumes:
      - ./jupyter:/home/myra/work
    environment:
      NB_USER: myra
      NB_UID: 1008
      NB_GID: 1011
      CHOWN_HOME: 'yes'
      CHOWN_HOME_OPTS: -R
```
* `volumnes` is how you map a local directory to a directory within the container, seperated by `:`
    >The username needs to match what is defined in the environment `/home/<username>/work`
* `environment` holds the details about the user of the notebook, it effects permissions of files created by the jupyter notebook platform. 