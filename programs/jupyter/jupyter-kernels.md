# Jupyter Kernels

## Print list of available kernels
```
jupyter kernelspec list
```

## Installing kernels
There are many kernels that have been created for Jupyter, a list of them can be found [on their github](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels). Most have their own documentation that you should follow.

### Installing .NET Interactive
>You can very easily add the languages of the `dotnet` core SDK as kernels to your jupyter notebooks. This adds `C#`, `F#` and `Powershell` as kernels.
>
>* You will of course need to have the _.NET core SDK_ installed first and _jupyter_ (which is included if you go with `jupyter-lab`).
>* Documentation: [Installing .NET Interactive as a Jupyter kernel](https://github.com/dotnet/interactive/blob/main/docs/NotebooksLocalExperience.md)
