# Basic: Using Jupyter
As mentioned, _Jupyter_ is essentially a in-browser text editor. You can create notebooks (they have the `ipynb` file extention), or any other text/programming files you need.

Code and data from files can be loaded into the _notebooks_, since they can execute code within _code cells_. 

## The Cells
The jupyter notebooks store their content as a _JSON_ file, segmenting everything into __cells__. You can have _code cells_ or _markdown cells_, depending on what you are putting in the document.

* ### Code cells
    > Program code is placed into the code cells and are executable, the output is printed below the cell. It is as if all the executed code cells form a single program for that particular notebook, so variables and values can be shared and overwritten between cells if the programming language permits it.
    >
    > Each notebook will be connected with a __kernel__ to run a particular programming language if it needs to execute any code cells. _Python_ is the default kernel. 
* ### Markdown cells
    >They use the normal __markdown__ syntax, but additionally they support __LaTeX__ code when placed within `$$`. This is probably my favorite feature.
* ### Raw cells
    >Preformatted text cells

## Running the cells
When you are writing into a cell, you can simply use  `ctrl + Enter` to run the code within the selected cell. 

There are easily accessable buttons that give you some options for the kernel and how it runs
1. Interupt kernel (stopping) 
    >Will stop any currently running code-cells (maybe they are taking too long)
1. Restart kernel
    >Will discard everything that has been run in the notebook (clean start for running cells)
1. Restart kernel and run all cells
    >Runs all the code cells in order

* ### Running Markdown cells
    >When using a _markdown_ cell, then it will display the processed markdown syntax.
* ### Running code cells
    > For _code cells_, it will add a number next to the cell to indicate when that cell was run last (like iterations). It helps tell you if you haven't run something, that another cell might depend on.  The code in cells that haven't been run essentially don't exist within the program that the kernel is putting together.

Hope that makes sense