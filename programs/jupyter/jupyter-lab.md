# Jupyter Lab
>I prefer the lab, so these notes are based on the usage of the notebooks within the `jupyter-lab`. These notes probably work for the classic notebooks as well (the interface layout might not be the same but their functionality are essentially the same).


### _Jupyter lab_ vs _classic notebook_
>The `jupyterlab` is the more advanced version of the interface, giving the impression of a proper editing program (such as VSCode) that keeps available files in a sidebar and everything is available within the same window.
>
> The __classic notebook__ seems more like github in appearance, initially showing you the available files in current directory (where you ran the jupyter server), and each file is simply a new page that you are directed to. 
>
> Besides for the presentation, the notebooks/files themselves seem to function identically.


## Kernels
>The notebooks use these kernels to access the programming languages to execute any code within the files.
>
>Since _Jupyter_ is created with _Python_, the initial language kernel available is the [IPython](https://ipython.org/) (`ipykernel`). 
>
>If you have other kernels installed, then you can easily change what kernel the notebooks are using through the _GUI_.
>
>* [Documents and Kernels](https://jupyterlab.readthedocs.io/en/stable/user/documents_kernels.html)
>* [List of available Jupyter kernels](https://github.com/jupyter/jupyter/wiki/Jupyter-kernels)

### Packages/Modules
> You will of course need to install into your system whatever packages you are going to be using in the code, if you wish that the kernel can run the code. It seems to work fine to install packages while the jupyter server is running.

### New files
>There is a chance that when you move a file somewhere into the directories where `jupyter-lab` is running, then the kernels might not notice it when referencing them (when loading the data from those files for example) even though the new files appear on the file index within the application. Those require a server restart. It seems to be better to use the _upload file_ within the application to move files into the directories while server is running if you don't want to spend time restarting the server.