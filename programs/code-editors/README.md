# Code Editors

* ### [CLI Text editors](../../cli-bash/README.md)
    > __VIM__ is a nice CLI text editor, any information about CLI is in another directory `cli-bash` from the root, since it's not a GUI text editor as the rest of these
* ### NotePad++
    > I used to use this for everything, the inbuilt ftp was useful
* ### [Visual Studio Code](vscode/README.md) (VSCode)
    > This is my favorite editor, with the huge collection of 3rd party extensions, you can get it to do just about anything that has to do with text editing in various languages (even render PlantUML and display Jupiter notebooks)
* ### Atom
    > It used to be a very popular choice, though I've never used it and apparently when I googled it, the top results were "is Atom editor dead?"

## Visual Studio Code (VSCode)
It is my top choice, but I just wanted to add a little section here about tips and tricks I pick up regarding this particular editor. 

If you have a favorite setup in the editor, then you can always make a backup of your `settings.json` 

You can sometimes customize some exmpansions through the `settings.json` 

### Various settings 
* Colorful Brackets
    > For years I have been using an extention (called _Bracket Pair Colorizer_) which color matching brackets so you could clearly see where they begin and where they end, since they'll be in different colors.
    >
    > It has now become a __built in feature__ of VSCode by the end of 2021. You enable it by adding the following line to your VSCode settings file
    >```
    >"editor.bracketPairColorization.enabled": true
    >```
* 