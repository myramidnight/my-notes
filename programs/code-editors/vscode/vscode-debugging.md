# Debugging 

### Basics of finding the debug `launch.json`

1. Open your `launch.json` to adjust your debug settings
   >![](images/vscode-debug-options.png)
1. Adjust those details for your debugger (depends on the programming language)

   
## Debugging Python
You might know that there are two ways to run python code.
1. As a program 
    ```
    python3 ./someProgram.py
    ```
1. As a module
    ```
    python3 -m someProgram
    ```
    > This actually refers to files same way as when you import them in python, using `.` dots instead of `/` slashes when indicating sub directories.

And you will also have to specify this in the `launch.json`, and you cannot have both.

### Debugging as a program
1. Open your `launch.json`
1. change the `program` to point at the file you wish to debug. 
    >The default is just to debug the selected file, so you don't really have to change this unless you want it to have a fixed program file it will run.
### Debugging as a module
1. You should open the project from the root of your program first (so things are relative to your program root, depending on how you organize things)
1. Open your `launch.json`
1. Remove the `program` line from configuration
1. You can use the __add configuration__
    1. Start writing `python` and press enter
    1. Then write `module` (select it when it is offered)
    1. lastly you write the name of your module, same way as you would specify it after the `-m` if you were running it in the terminal:
        ```
        python3 -m src.mymodule
        ```

## Debugging C++ (in WSL)
The debug part of the information might not be exclusive to WSL environment, but it is good to keep in mind that it is being done in such a linux environment.

* [Video guide](https://www.youtube.com/watch?v=1HGBk78BqR4)
    >So basically you need to compile your program with debug flags, so it's handy to simply have a recipe in a __makefile__, so you can run `make debug` and then proceed debugging it.  Apparently it won't really notice your breakpoints when debugging unless you do this.
    >* Just remember to point your debug configs to the debug-version of your program


1. Compile your program with debug flags
    > This particular setup of flags is strict on warnings, such as unused variables or extra `;` 
    ```
    g++ -std=c++11 -Wconversion -Wall -Werror -Wextra -pedantic -g3 -DDEBUG -o progName main.cpp
    ```
1. Adjust your `launch.json` details to point at your debug program that you just compiled
    >If the program takes arguments, you add them to the `args` list
   >
   >![](images/vscode-debug-launch.png)
   >* If you want it to stop at the beginning of the program, then set `stopAtEntry` to be true.
1. After saving your options, you can run the debugger (pressing the green arrow) and it should now stop on all your breakpoints.