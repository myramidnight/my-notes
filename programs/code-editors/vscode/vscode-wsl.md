## Combining Windows and Linux!
If you have virtualization on your computer, perhaps using it to run the WSL Ubuntu terminal. You can actually launch your VSCode editor to be using WSL.

* ### [Run VSCode in WSL](https://code.visualstudio.com/docs/cpp/config-wsl#_run-vs-code-in-wsl)

    Ever notice the `><` icon in the corner? if you right-click on it, you get interesting options, such as re-opening the folder with WSL
    * <img src="images/wsl-status-bar.png" style="width:400px">

    * Just remember, the WSL ubuntu is essentially a different system, so extentions you might have installed on windows will not be on the Ubuntu. 

