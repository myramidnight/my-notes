# The Geany IDE editor
A very nice light-weight code editor that I like to use on the _raspberrypi_.

>Currently the latest version of geany I can install using `apt install` is 1.33 (something to do with how Debian does things, and Raspbian is Debian based).
>
>The markdown plugin wasn't done upgrading for that version before Debian released it's list of latest stable packages. So apparently you will need to get the source code and compile it yourself if you want anything that wasn't in that Debian release.

Well, it seems to depend on what OS you are using, and what packages you have installed, if you can compile the program or it's plugins from the source. Even installing it with `apt` varies, didn't know the OS had a hand in restricting access to what you can install. 

Really like this editor, specially when you need something light weight for a little thing.