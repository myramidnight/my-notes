# Docker Compose
With the use of a `yaml` file, we can start up and manage multiple containers with a single file, and it also handles tearing down the containers.

* Run all the containers from the docker-compose file
    ```
    docker-compose up
    ```
* Run only a single service, and the containers it depends on. It goes by the names you give the services in the file.
    ```
    docker-compose up <service-name>
    ```

* Tear down the containers created with this compose file
    ```
    docker-compose down
    ```
* Remove the volumes of this composer, can specify a individual service for this command as well. Volumes are not automatically removed if you tear down the containers.
    ```
    docker-compose down --volumes
    docker-compose down <service-name> --volumes
    ```
    >Handy when you're need to quickly remove persistent data