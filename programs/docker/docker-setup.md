# Docker: Setting up base
> These notes are composed with Windows 10 in mind

First of all, you have to keep in mind that Docker is essentially a virtual machine, and it demands that you have __virtualization__ enabled on your computer.

Easest way to quickly check that on Windows is to open your __task manager__ and look in the _performance_ tab. There you should see plainly printed "Virtualzation: Enabled" or "Virtualization: Disabled". 

### Requirements of Hyper-V (Hyper-vicer)
>[Microsoft quick-start guide to Hyper-V](https://docs.microsoft.com/en-us/virtualization/hyper-v-on-windows/quick-start/enable-hyper-v)
>* Windows 10 Enterprise, Pro, or Education
>* 64-bit Processor with Second Level Address Translation (SLAT).
>* CPU support for VM Monitor Mode Extension (VT-c on Intel CPUs).
>* Minimum of 4 GB memory.
>
>Yes, it does not include __Home__ edition. 

* [Running Docker without Hyper-V](https://poweruser.blog/docker-on-windows-10-without-hyper-v-a529897ed1cc) might be worth while. I haven't tried it, I probably should
* __WLS2__ still uses _Hyper-V_ (it's not an alternative, it's an improvement)

## Docker on windows
![](images/docker-k8-windows-requirements.svg)
>I wish I had found this little map when I tried to install docker the first time
* [Installing Docker and Kubernetes on Windows](https://learnk8s.io/installing-docker-kubernetes-windows)

### Docker desktop
## Docker desktop
The Docker desktop app is quite nice, shows you what images have been built and containers are running. Makes it very easy to stop/run or remove any of them (can of course not remove an image that is being used by any container).

## My installation experience
I actually have a rather bothersome experience getting Docker to work, did it the previous year on my laptop (Win10 home edition) and now on my Win10 Pro desktop computer. But I did manage to get it to work in both cases (laptop later started to get weird, and I had to factory reset it, since the BIOS settings had disappeared...).
> [You can read about it here, if curious how I got through the installation](docker-win10-experience.md)
