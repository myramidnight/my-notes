
# Dockerfile (the docker instructions)
These are the instructions for creating the container for any project. Once you have all the instructions in place, then you can try to build the docker image (which can then be run as a container).

* The build will look for any file named `Dockerfile`


## Dockerfile (image instructions)

You can add options in the build command, such as adding a reference tag so you don't have to reference to the image by full ID. 
* Each project and framework will require special instructions in order to work, you might find ready templates for it.
* Have to tell it what files to copy and what commands to run in order to make the project ready within a container.
* The `WORKDIR` tells you where in the container you are running any commands. 
* You can actually find ready docker images on the dockerhub.com to create containers from.
* You have to expose ports that will be used (if you want the container to have open ports outside of itself)
* There is always a chance that you have to clear the cache if you're unable to attempt build image after it fails once (it'll be obvious if you're having this issue). Easy to clear it from the docker desktop.

### Setting up a Dockerfile
```dockerfile
# syntax=docker/dockerfile:1

FROM python

WORKDIR /app

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY . .

CMD ["python", "src/main.py"]
```
* the `COPY . .` needs to have that space in between, because it's actually two arguments (source and destination).

## Docker layers

## Multi-stage builds

## Dockerfile best practices
* Use _Multi-stage_ builds
* Leverage build cache
* Use `.dockerignore`
* Understand the build context
* Reduce number of commands that build new layers, such as combinging `RUN` commands.