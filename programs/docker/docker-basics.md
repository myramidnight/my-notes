>* [difference between dockerfile, docker-image and docker-container](https://nickjanetakis.com/blog/differences-between-a-dockerfile-docker-image-and-docker-container)
# Docker basics
First we need to understand the various ways we can host applications, each being an advancement on the previous. Each has it's own advantages and purpose, so they are not obsolete. 
* ### Dedicated machines
    >A single machine to host a single application. This takes alot of physical space (though normally such server machines do not have individual keyboards or screens, accessed over the network).
    >
    >This is simply a computer, can do anything you can do on your own private machine (depending on the hardware of course)
* ### Virtual Machines (VM)
    >In order to utilize a single machine better for hosting applications, we can create virtual machines, emulate _dedicated computers_, while in reality they are sharing the hardware. Saves on the hardware space. But each has their own copy of the Operating System they use, and any resources they require.
    >
    >![Virtual machiens](images/virtual-machines.png)
* ### Containerized Aps
    >Docker is essentially a single VM that can create multiple containers, each essentially emulating a single _VM_, that can share resources. The only thing each of them will contain is the program and their libraries. 
    >
    >A lot of space is saved that normally would be wasted on duplicate OS installations in VM, so we have more space for more containers instead.
    >
    >![Containerized apps](images/containerized-apps.png)

## Advantages of Containerization
* #### Save space (better hardware utilization)
    > Since containers do not need to install anything other than the libraries used by the application, they take a lot less space than traditional VM.
    >
    >Space for more containers!
* #### Sharing resources
    > Docker just needs to have a single copy of each of the reasources: OS, kernels, core libraries. Each of the containers can share these resources.
* #### Speed in deployment
    >Since the containers share the resources, they do not need to waste time on installing those resources themselves, since they are already in place. Only need the time that it takes to install the libraries and starting up the program.
* #### Reliable development enviornment
    >Developers can easily test and run their code in identical enviornments with the use of containers. This eliminates the bother of "works fine on my machine" issue.
* #### DockerHub: Collection Official images
    >You get access to a collection of official docker images through __docker hub__. Then you don't have to worry about dubious 3rd parties for base images.

## Creating containers
All containerized platforms are essentially the same, so the concept applies to containers in general (each platform might have different names and syntax for these steps/formats, but it's the same idea).

Here we are only looking at the docker platform.
### The steps
1. #### Create the dockerfile
    >The `dockerfile` contains the instructions of how to create a _docker image_ when the file is built.
1. #### Build the dockerfile (create image)
    >This creates the docker image
1. #### Run the docker image (create container)
    >We call upon the image by name and run it, to create a container. 
