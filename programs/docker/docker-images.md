
# Docker images

## Build docker image (terminal command)
```c#
# docker build <options> <dockerfile-location>
docker build -t my_project ./
```
We run the `build` command on a `dockfile` to create a image, we might not need to specify it by name, but we need to specify in which directory it is. 

```
docker build -t <name-of-image> <location-of-dockerfile>
```
* The image name needs to be lowercase
```
docker build -t my-docker-image .
```
* Here we're just going to build a image from a `dockerfile` located in current working directory.

## Run the docker image (create container)
When you have a successfully built image, then you can run it as a container. 

* Containers cannot communicate with the outside unless you map the exposed ports to external ports.
* You can give the container a name, else it will just randomize it.
```C#
# docker run <options> <image id/tag>
```