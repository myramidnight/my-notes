# Docker Compose
It can be very tedious when developing your application with docker, when you have to manually remove the container every time before you can re-run the same application with changes.

`yaml` file allows you to manage the container and it will also tear down the container for you when you wish to re-run the application, making things a lot simpler for you. Essentially making re-usable containers.