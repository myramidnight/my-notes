# Docker network
Containers cannot talk to each other or anything outside of themselves unless we expose ports

Each container is assinged a IP address, which can be found by _inspecting_ the container. It is a very crude way to hardcode this IP into the code though, because it relies heavily on the containers being created in specific order (each container increments the IP used for the next container)

We can create a _network_ within docker

## Create network bridge 
* `docker network create <network-name>`
    >Creates a new network within docker
* `docker network ls`
    >lists the existing networks
* `docker network inspect <network-name>`
* `docker run --network <network-name> <image-name>`
    >Will create a container within the given network

### Allowing containers to talk together
All containers running on the same network can talk together, and the network name replaces `localhost`. 