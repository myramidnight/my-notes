# Commands for docker
Can always check what a command does (or find out the subcommands) by writing `--help` after the command

* ### Interacting with different types of items
    >You can specify what you want to manage with your commands
    ```
    docker container ls
    docker volume ls
    docker image ls
    ```

* ### List items (`ls`)
    >Aliases: `ls`, `list`, `ps` (exclusive to containers)
    ```
    docker ps 
    docker container ps
    docker container list
    ``` 
* ### Remove (`rm`)
    * #### Removing all containers
        ```
        docker rm -f $(docker container ps -a -q)
        ```
    * #### Remove all volumes
        ```
        docker volume rm $(docker volume ls -q)
        ```
* ### Build image
    ```
    docker build -t <image-name>
    docker build --tag <image-name>
    ```
    >Tag names the image, but each image needs a unique name

* ### Run container
    ```
    docker run <image-name>
    docker run --name <container-name> <image-name>
    docker run --name <container-name> --network <network-name> <image-name>
    ```