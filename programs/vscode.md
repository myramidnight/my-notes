[back to index](./README.md)
# Notes about `Visual Studio Code` (VScode)
Just handy little things I've picked up. Using a windows machine and Icelandic keyboard layout.

## Opening a new project
I find it handy to first create the directory that will contain your project, then open a new window in VScode and simply drag the directory onto the new window. This will open the directory as a `new workspace`. 

The sidepanel will then only list the contents of current workspace.

## The inbuilt terminal
> The shortcut to open the terminal is `ctrl` + `æ` or go to `View > Terminal` to open it (it even shows you the shortkey to access it). 

The terminal will automatically be set to be in the root folder of the project, which is quite nice.

You can even open `multiple terminals` right there by pressing the `+` next to the dropdown menu and have it run different things that usually locks down the terminal while running, such as `SASS` compiler. 

To go between different terminals, use the dropdown menu. The terminal name in the dropdown will tell you what it is running.

## Split editor/screen
> Simple way to initiate a split screen is to drag a tab over to one side of the window.

This lets you view different files side by side, or different sections of the same file (same file can be opened multiple times for this purpose)

## Favorite extensions
* [TODO tree (tool)](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.todo-tree)
    > It finds all the `TODO` comments in your code, and then it lists all the files that have it in a very neat way, easily accessable and includes the message you wrote with the task.
* [Night Owl (theme)](https://marketplace.visualstudio.com/items?itemName=sdras.night-owl)
* [Bracket pair colorizer (styling)](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
    > Well, who doesn't like a colorful div soup, I mean brackets. 

## Quickly commenting out
* `Ctrl` + `+` will toggle between commenting out or uncommenting the selected code.