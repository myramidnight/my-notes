> Because I really find it tedious to google the same thing on repeat
# Pytest and Unittest
These two work wonderfully together, and here I will collect a few neat tricks I've picked up.

## Using TestCase
The big thing about using __TestCase__ would be how it lets you figure out what test failed easier, having multiple TestCases in a single file when you are testing similar things but want to categorize them a bit.

### Adding class attributes
You don't really initiate the TestCase, but you can do the following to add your own class attributes
```py
from unittest import TestCase

class MyTestCase(TestCase):
    def setUp(self) -> None:
        self.myAttribute = "whatever I want"

    def test_setup(self):
        assert self.myAttribute == "whatever I want"
```

## Mock Class
Creating a Mockup of a real class will create a fake class to interact with, but keep mind that it's a fake. Even if it will allow you to call and interact with the Mockup as if it were the real thing, it will not do anything

These are specially useful when you are testing classes that have requirements (passing it other classes on initiation) and don't want to bother with the whole initiation chain of classes needing classes. A Mock class doesn't require initiation, because it's not real. So feed the real class some mockups.

### Return mock values
You have to manually set the reactions, so if you require a returned value, you have to specify it.

```py
from unittest.mock import Mock

class RealClass():
    def get_string(self):
        return "My String"

# create the mock
mock_class = Mock(RealClass)
mock_class.get_string.return_value = "Mock String"

print(mock_class.get_string()) #prints "Mock String" 
```

### Raise mock exceptions
* [Check if exception was raised (or not)](https://miguendes.me/how-to-check-if-an-exception-is-raised-or-not-with-pytest)

Because sometimes we want exceptions, or want something to happen that would raise an exception
```py
import pytest
from unittest.mock import Mock

class RealClass():
    def raise_exception(self):
        raise Exception("Real Exception")

# create the mock
mock_class = Mock(RealClass)
mock_class.raise_exception.side_effect = Mock(side_effect = Exception("Mock Exception"))
```
### Then we test for exceptions
>This is not specific for mockups, but I decided to use the mock class anyways
```py
# Then we test for exceptions
with pytest.raises(Exception) as mock_exception:
    mock_class.raise_exception()

assert mock_exception.type == Exception
```

or we could just use a __try-except__ block
```py
try: 
    mock_class.raise_exception()
    assert False
except Exception:
    assert True #because we wanted an exception
```

## Catch output

```py
from unittest import TestCase

class MyTestClass(TestCase)
    @pytest.fixture(autouse=True)
    def capsys(self, capsys):
        self.capsys = capsys

    def test_output(self):
        print("Hello!")
        captured = self.capsys.readouterr()
        assert captured.out == "Hello!\n" #because output ends with a line break

        #you could use this too:
        # captured, err = self.capsys.readouterr()

```