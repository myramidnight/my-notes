# Python packages
Packages that I've found useful.

## For statistics
### Matplotlib
>* [w3resource Matplotlib](https://www.w3resource.com/graphics/matplotlib/)
>
>It is a comprehensie library for creating static, animated and interactive visualizations in python.
>
>Works wonderfully in _IPython_ used by __Jupyter__ notebooks to present your data.

### NumPy
>* [w3resources NumPy](https://www.w3resource.com/numpy/index.php)
>
>NumPy is the fundamental package for scientific computing in Python. The library provides a multidimensional array object, fast operations on arrays and basic linear algebra among many other things.

### Pandas
>* [w3resource Pandas](https://www.w3resource.com/numpy/index.php)
>
>Pandas is an open source library providing high-performance, easy-to-use data structures and data analysis tools for Python. It can also visualize the data.