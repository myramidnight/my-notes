# Vector/List comprehension with python
> Simplest explanation would be that list comprehension is a simple way to create a list that loops through another list or range. A one-liner that generates a list.

List comprehension is an alternative method of writing `for` loops (and similar) functions that is readable, quick, and easy to use.

It allows you to create powerful functionality within a single line of code.

> List comprehensions are a third way of making lists. With this elegant approach, you could write the `for` loops with just a single line of code

```py
squares = [i * i for i in range(10)]
# [<do something> for <item> in <list/range>]
```
is like writing the following for loop
```py
squares = []
for i in range(10):
    squares.append(i*i)
```

both will create the list `squares = [0,1,4,9,16,25,36,49,64,81]`

## List comprehension with dictionaries
You can write _list comprehensions_ that iterate over the keys or the values of a dictionary, using the following dictionary methods to get something you can iterate through:
* `keys()`
* `values()` 
* `items()`

>You technically have to wrap it in `list()` in order for the returned list to be an actual list (else it's technically a `dict_keys`, `dict_values` or `dict_items`). It's good to know this if you want to store the lists from those methods in variables.
>```py
>>>> {4: 'a', 3: 'b'}.values()
>dict_values(['a', 'b'])
>
>>>> list({4: 'a', 3: 'b'}.values())
>['a', 'b']
>```

While `keys()` and `values()` simply returns a list of either the keys or values, the `items()` will return a list of tuples, where the keys and values have been paired together.



```py
[2*x for x in {4: 'a', 3: 'b'}.keys()]
# [8,6]

[x for x in {4:'a', 3:'b'}.values()]
# ['a','b'] 
```
* First example just gets a list of keys, and generates a new list where all the values are 2*key for each key in the given dictionary.
* later example does nothing to the values from the dictionary, other than just creating a list with only the values (because nothing is done to the `x`)
* You could call the variable anything you like, it could have been called _item_ instead
    ```py
    [item for item in {4: 'a', 3: 'b'}.keys()]
    ```

if you would be using the `items()` method on the dictionary, then you are iterating with two variables
```py
[key*value for key, value in {4: 'a', 3: 'b'}.items()]
# ['aaaa', 'bbb']
```
* Here I combine the keys and values to generate a list of strings
* Since python is pretty clever, it assumes that since we are trying to multiply a string, that we want the outcome to be a string (since the key and value aren't of the same type). When a string is multiplied, then it is just concatinated with itself essentially.