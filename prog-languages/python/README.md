# Python
> I use `py` to call my python installation, you might be using `python` or `python3`, it just depends on how you set things up.

The more I use python, the more I like it. Just like with any other programming languages, it takes a bit to get into, but it's also a very good beginner language if you want to start on a higher level (the higher the level, the more the language simply does for you, such as memory management)

## Index 
* [Basics](py-basic.md)
* [Opening files](py-files.md)
* [pip (installing packages)](py-pip.md)
  * [Interesting packages](README-packages.md)
* [Virtual Enviornment (venv)](py-venv.md)
* [Testing (with pytest)](pytest/README.md)
* [Tkinter](tkinter/README.md)
  >It is a GUI library that is actually part of python standard library


## Extra
> Unsorted things
### Assert statements
So the `assert` keyword is not confined to testing. You use it to assert if a statement is true or not, and it will only continue the program if it happens to be true.

So anything below the assert will not run if false, only return a `AssertionError`
```py
x = 10

assert x > 0
print('x is a positive number')
```
you can give them a error message
```py
x = 0

assert x > 0, 'Only positive numbers allowed'
print('x is a positive number')
```
* This will print out "Only positive numbers allowed" before exiting, never going past the assert.
* If you don't want the assert to end the program, you can wrap it in a `try-except` block, which will catch the error and you can handle it however you want.
