# ESlint
The tool to keep all your ECMAScripts in line. Create a configuration file, set the rules and let ESlint keep the coding standards.

## Initialize 
First you need to initialize `eslint` and tell it how it should behave and the rules it will have.
```
eslint --init
```

## Setting rules
This is really useful for JavaScript because it's such a loose language, too many ways to write the same thing, which can lead to a very messy code if things are not consistent. ESlint gives you the power to set rules that have to be followed, such as the indent size, use only double quotes for strings and always end lines with `;`, just to name a few possible rules. ESlint will raise any style violations as problems even if the code might function just fine.

### Fixing the code
But it could be tedious to be importing code from elsewhere and it isn't following your rules, nobody wants to manually have to change all single quotes to double quotes or vise versa. ESlint got you covered there too, because it can just `fix` it for you. 

These are just style fixes, things that do not effect the functionality of the code but will make the project feel consistent.

## Running eslint through `npm` script
Apparently after eslint finishes running, it will exit with `1`, which will cause `npm` to render a error message, making it seem like there are always issues with eslint.

The following script could be what you use to run eslint in current project. To run it you would call `npm run lint`
```json
{
    "scripts": {
        "lint": "eslint ."
    }
}
```
But as mentioned before, eslint will cause `npm` to print errors when there isn't anything wrong. You can silence the errors by adding `-s` to the command
```
npm run lint -s
```
