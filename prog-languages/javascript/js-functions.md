# Functions in Javascript
* [Mozilla reference: Functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions)
## Function Syntax
### Basic syntax
* Declaired function (been given a name)
    ```js
    function myFunc(param){
        //function code
    }
    ```
* Anonymous function (assigned to a variable)
    ```js
    const myFunc = function (param){
        //function code
    }
    ```
### Arrow Functions (since `ES6`)
```js
const myFunc = (param)=>{
    //function code
}
```

It is good to keep in mind the limits of _arrow functions_. Probably best to just treat them as __anonymous functions__.
* They do not have their own bindings to `this` or `super`, and should not be used as methods.
* They are not suitable for `call`, `apply` and `bind` methods, which generally rely on establishing a `scope`. 
* Cannot be used as __constructor functions__.
* Cannot use `yield` within it's body.
* Read more at [MDN: Arrow function expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)


## Declaired vs. Anonymous functions
* The name of a __decalaired function__ cannot be changed, while a function in a variable can be reassigned. 
* A function name can only be used within the function's body, and will be `undefined` outside of the function scope.
    * Therefor a declaired function cannot be contained in a variable.

