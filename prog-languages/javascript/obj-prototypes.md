* [w3schools: js object prototypes](https://www.w3schools.com/js/js_object_prototypes.asp)
* [medium: factory functions vs. constructor functions](https://medium.com/@erinlejhimmy/factory-functions-vs-constructor-functions-in-javascript-8bef9510be3c)

# Javascript Object Prototypes
A prototype is a _blueprint_ for an object, which are produced by __constructors__ that generate new instances of objects. This is part of the __factory pattern__ in _object-oriented-programming_. 

Every object has a prototype except the core __Object__ which every object inherits from.

## The Constructor Function
You can create a function that produces an _object_ that can be populated with values through it's parameters. 

```js
function Person(first, last, age){
    this.firstName = first;
    this.lastName = last;
    this.age = age;
}
```

This `Person` function is now what we call a __constructor__ that creates new instances of a prototype object. 

* The `this` keyword will refer to the current instance of the object.
    > You use `this` within constructors and methods to make sure you are referencing the current object instance, rather than pointing to something that just exists in the constructor.
* You call that constructor with the `new` keyword to create a new instance of the prototype, which will inherit it's properties and methods.
    >```js
    >var boy = new Person("Harry", "Potter", 11);
    >console.log(boy.firstName) //output: "Harry"
    >```

* You __cannot__ add new properties to a object constructor the same way you add new properties to existing objects. The constructor isn't a mutable object, it is a function. All properties and methods need to be defined within the _constructor_.
    >```js
    >Person.nationality = "English"; //This will not work
    >boy.nationality = "English"; //This gives boy a nationality
    >```