* [w3schools - javascript array reference](https://www.w3schools.com/jsref/jsref_obj_array.asp)

# Array Methods

### Array.reduce
> This method executes a reducer function (that you provide) on each element of the array, resulting in a single output value
>```
>arr = [1,2,3,4]
>func = (total, num)=>{return total + num}
>
>arr.reduce(func) // output: 10
>```

### Array.map
### Array.filter
### 