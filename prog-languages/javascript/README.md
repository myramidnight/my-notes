# JavaScript
Despite __Java__ being part of the name, __JavaScript__ is not related or based on it. It is actually said that the developer of it was trying to make _Schema_ (language in the _Lisp_ family) look more like _Java_, thus __JavaScript__ was born.

### ECMA-Script
>You might hear ECMA-Script (or ES) and JavaScript being used interchangably, but ECMAScript are the standardized versions. Reason for the name is interesting, and I could roughly say the cause is an argument between Microsoft and Netscape over who owned the name JavaScript. 
>
>It is nice that there is a shared standard base code in all browsers, features of any version of ES are supposed to be working in any version of JavaScript that is built ontop of it. Different browsers actually develop their own JavaScript it seems, building new features that might perhaps become a part of a future ES version. Back in the 'old days', things tended to look and function differently based on what browser you used, which was a big issue for frontend developers trying to make something look good on different browsers. People tended to just focus on one browser and tell people what browser the website was developed for, which is thankfully not a thing anymore (which is probably thanks to CSS and ECMAScript together, developers no longer leaving anything to the default browser styling).
>
>[Brief history of JavaScript and ECMAScript](https://medium.com/@madasamy/javascript-brief-history-and-ecmascript-es6-es7-es8-features-673973394df4)

### Interesting links
* [You Don't know JS (1. edition)](https://github.com/getify/You-Dont-Know-JS/tree/1st-ed), as published by O'Reilly
    > Git repo for this series of books/chapters, their [2nd edition](https://github.com/getify/You-Dont-Know-JS/tree/2nd-ed) is being developed in that repository.
* [Fun Fun Function (youtube)](https://www.youtube.com/channel/UCO1cgjhGzsSYb1rsB4bFe4Q)
    > A great source of knowledge about programming concepts and being a developer. JavaScript (ES6 +) is the meta-language for this channel (used to explain the concepts).
* [JavaScript Equality Table](https://dorey.github.io/JavaScript-Equality-Table/)

## Notes index
### JavaScript in general (ES6)
* [Array methods](array-methods.md)
* [Prototypes](obj-prototypes.md)
### [NodeJS](nodejs/README.md) (The JS engine outside of browser)