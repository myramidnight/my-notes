# Programming languages
It is good to have some notes about different programming languages you learn, to help you back into the language whenever you pick it up, because it almost feels like each course or semester wants to have you using different languages each time.

They are similar enough to get into, but unique enough to give you a headache when jumping between them.

> There are so many programming languages that are basically making communication with the computer more _human friendly_. We can categorize these languages into levels, with `Python` and `JavaScript` being rather high level while `C` is considered low level. These levels do not describe their difficulty directly, best is to think "_how close to the ground is the language?_" with the ground being the assembly. 

### Scripting languages?
Essentially all _scripting languages_ are __programming languages__, but not the other way around. Most high-level langagues are infact __scripting languages__.

>Typical scripting languages are intended to be very fast to learn and write in, either as short source code files or interactively in a __read-evaluate-print__ loop in a shell/terminal. This generally implies relatively simple syntax and semantics.
>> [Scripting language - wikipedia](https://en.wikipedia.org/wiki/Scripting_language)


### Frameworks
You should check out frameworks if you don't want to spend time re-inventing the wheel in order to start working on your project. Frameworks are code libraries created in specific languages.
* #### [Framework libraries](../prog-frameworks/README.md)

## Index of language notes
These are most of the languages that I've had to pick up. These notes tend to just be the basics or good to know tips/tricks that I've learned, because it can be difficult having to jump between languages and finding all your previous references to get back into things. 
### High level languages / Scripting languages
The higher the level of a language, the more it does for you in the background, such as memory-managment and garbage disposal. Most high-level languages are infact __scripting languages__.
* [__Python__](python/README.md)
    >It is designed to make software programming easier, and is actually a very good language for beginners in programming.

* [__JavaScript__](javascript/README.md)
    > __Java__ and __Javascript__ are completely unreated languages. 
    >
    > It is the Programming language of the webbrowsers, essential for those in web-development.
    >* [__NodeJS__](javascript/nodejs/README.md) (extracts the language engine for use outside of browser)
    > * [__Typescript__](javascript/typescript/README.md) is Javascript with syntax for types
* __PHP__
    > Stands for __PHP: Hypertext Preprocessor__ (originally "_Personal Home Page_"), and is a language especially suited for web-development and allows you to combine __HTML__ and programming (for example: _show this __if__ condition is met_). 
    >
    > The evaluation of PHP happens server-side, so it delivers a static webpage to the browser (which can become dynamic with the use of __JavaScript__ client-side)

### [The C programming languages](c-languages/README.md)
>`C` is a very low-level programming language, and there exist [many different derivatives](https://en.wikipedia.org/wiki/List_of_C-family_programming_languages) of it. Some have improved so much that they aren't even considered to be _low level_ anymore. 
>
>* C
>* C++ (cpp)
>* C# (c-sharp).

### Lower level languages
> When learning computer-science, a lower level language is often preferred when teaching about the concepts, hardware and memory. 
>
> _Out of sight, out of mind_. How can you appreciate some of the things higher-level languages do for you, when you have never had to consider how much work you would have to do yourself othervise? Memory managment, garbage disposal, infering datatypes...  
>
> The lower the level, less the language does for you. But that also means you gain more control over what happens, because you have to implement it yourself.
* [__Java__](./java.md)
* [__F# (f-sharp)__](./f-sharp/README.md)
    > It is a __functional-first__ general purpose language. It is very strict about typing, but is a high enough level to be able to infer datatypes without you having to declair them.
    >
    >* It comes with the __.NET__ core and can also use any of the `nuget` packages for that framework.
    >* It can also be used as a _scripting language_ with the use of `fsi` in terminal/shell. Then it uses `;;` as line-endings to tell `fsi` to run the line/code-block.

