# The `C` programming language family
Since they all belong to the same family, the structure of the code will be familar, how you compile them, and the general syntax.

There are many other languages that are made to improve upon `C` (they tend to be called _C something_). Here are notes about the ones I have actually tried.
## Index
* ### [C](c.md)
    >`C` is a very low-level programming language. I personally have only used in courses that want you to gain understanding of how the hardware or core software works (things close to the hardware and system core). 
* ### [C++](cpp/README.md)
    > `C++` (cpp) extends the functunality of `C` quite a bit, making it easier to use as a programming language, but doesn't pull it out of being a low-level language.
* ### [C#](c-sharp.md)
    >`C#` (c-sharp) however improves the language so much that it's been labeled a high-level programming language. It has alot of overhead and libraries that need to be included before it will compile. The core of `.NET` is built with `C#`.