
# Strings
These are essentially arrays of characters. 
```cpp
char my_first_word[12] = {'f','o','r','r','i','t','u','n'};
char my_second_word[12] = "forritun";
char my_third_word[] = "forritun";
```
All of those will create string, and if you print them you get "forritun". But for the first example, if we have the array lenght exactly the number of characters, things will get weird. It reads until it finds the end-string character, an escape character `\0`, a zero essentially. Like with the int array, if we specify some of the values, the remainder defaults to zero, which works for char arrays (so it doesn't read the empty spots of the array that we didn't fill with characters of a string). 
```cpp
// This will print out weird, because there is no space for a null character at the end. It will continue to read until it finds the escape character it hopes for (so it reads into memory that wasn't assigned to it)
char my_first_word[8] = {'f','o','r','r','i','t','u','n'};
```
Strings in C (and later) are technically of length size of word + 1 (for the escape character).