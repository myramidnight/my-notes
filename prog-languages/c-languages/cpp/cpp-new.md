
## The `new` keyword
The `new` keyword attempts to allocate and initiate an object or array of a specified or placeholder type, and returns a suitably typed, non-zero pointer to the object (or to the initial object in the array). 

```cpp
int * array;
array = NULL; 
```
* Here we declare a pointer of type `int`, and name it `array` (could have been named anything). 
* This could actually just point at a single integer, but this also lets us point at the head of an array. So technically when we use `int * varName`, it could be considered to be an array of size 1, because the head of an array is also the value of the first item.
* Since it's a pointer, and we didn't give it any value, it isn't actually pointing at any valid location. We give it the value `NULL` to simply make it clear that it's a pointer that isn't pointing at anything.
```cpp
array = new int[8]; //another syntax to create an array on the heap
```
* Here we define an int array of size 8 to store in the pointer named `array` (yeah, maybe that variable name might be confusing...)
* If we use `new` when defining variables, then we are creating them on the stack
    >That means that allocated memory will persist even if we leave the scope.
* It is important to delete all instances of `new` when we're done with them
    >Else we might end up with a _memory leak_, where we keep allocating memory and never releasing it even though we aren't using it anymore, until we simply run out of memory.
    >
    >Though once we exit the program, leave the `main` scope, the computer itself will handle releasing all the memory again, but we have to manage our memory while the program is running, to release anything on the heap we aren't using.
