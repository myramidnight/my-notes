# Reference and Pointers

>The performance of pointers and reference is exactly the same, since _reference_ are implemented as pointers

* A _pointer_ in `C++` is a variable that holds the memory address of another variable. 
* A _reference_ is an __alias__ for an already existing variable.
    >Once a reference is initialized to a variable, it cannot be changed to refer to another variable

## Reference
It might be confusing to call it a _reference pointer_, but behind the scenes _reference_ are implemented with pointers. The difference is that you cannot change what a _reference_ is pointing at after you have declared it (someone described it as a _constant pointer_, which is valid, since constants cannot be changed)

```cpp
int a = 4;      //int variable
int b = 3;      //int variable
int& r_a = a;   // reference to a int variable 'a'
//'r_a' is now just another name/alias for 'a'

// r_a  = 4
// a    = 4
// b    = 3
```
* The `&` announces that the variable `r_a` is a reference, and it's value is the variable it is referencing.
    > if we change the value of `r_a` at any later point, it will simply be effecting `a`
* It doesn't actually matter if we write `int& r_a`, `int &r_a` or `int & r_a`, just that the `&` is between them
* Simply put: we just made an alternative handle for variable `a` called `r_a`
```cpp
r_a = b;    //changing the value of r_a
// r_a  = 3
// a    = 3
// b    = 3

r_a = 9;
// r_a  = 9
// a    = 9
// b    = 3
```
* This will change the value of `r_a` to have the value of `b`
    >We are not actually changing the reference itself, just the value of whatever it is referencing
* Since `r_a` is just a reference, we just changed `a`

### Why do we want to use Reference?
> Why would we want to use _reference_ if we can just use the variable it is referencing? Seems redundant

## Pointers
Let's continue with the reference and pointers
```cpp
int * p; //creating the pointer
```
* This creates the pointer `p` that points at a `int` variable somewhere on the heap
* This specific declaration hasn't specified a value, so it's current value is just some junk (whatever was previously allocated in the memory at that spot where the pointer is stored)
    >This isn't really good, because the program might try to translate that junk binary as an actual address, resulting in errors most of the time or weird outcomes.
* Again, same as with reference, it doesnt matter if you write `int* p`, `int *p` or `int * p`
    >It's essentially about preference, or perhaps coding guidelines (for linting and consistency, cosmetic)
### NULL pointers
```cpp
int * p = NULL; 
```
* It is actually considered good practice to set a new pointer to be `NULL`, `nullptr` or `0` to indicate that it's not actually pointing at anything (not in use) 
    >This makes it easier to identify if pointer contains a real value or not. 
    >
    >And it allows you to make conditional statements to do something in case the pointer is _NULL_
* When the pointer is of a number type (`int`, `float`...), then it can be a bit misleading to use `0` as the _null_ indicator, since it will simply look like it's storing the number `0`
    >it essentially makes it more confusing to read your code, simply stating `NULL` isn't going to be misunderstood
### Memory addresses (pointer values)
```cpp
p = &b; //Taking the memory address of 'b' and storing it in 'p' 
```
* When we use `&` in this case (as part of the value), then we are actually taking the memory address of variable `b` and storing it in the pointer `p`
    >So this is not the same as the reference (which is before the variable name when declaring it). Could perhaps think: if it's used with an existing variable, then it's a memory address!
* This will give the previously created pointer `p` the memory address of variable `b`, so now `p` points to wherever the value of `b` is located.
* `p` doesn't actually reference `b`, so if you change the value again, you are changing the pointer, not the value of `b`

### Dereferencing (Interacting with the actual value)
Dereferencing is a way to get the actual value (not the memory address) from a pointer, to interact with whatever the pointer is pointing at.
```cpp
*p = 45;
```
* This essentially reads "_whatever `p` is pointing at, change it to `45`_"
* If we had stored the memory address of `b` in `p`, then this would be the same as doing `b = 45`
* Anything that is pointing at that position in the memory will now have the value `45`
* This is essentially what a _reference_ is, a dereferenced pointer
    >So maybe if you're doing this a lot, then it might be simpler to just use _reference_
### When to use pointers?

The main thing we should be thinking about is "_do we need dynamic memory allocation?_"

>Always use appropriate tool for the job. In most all situations, there is something more appropriate and safer than performing manual dynamic allocation and/or using raw pointers.

#### When do we need dynamic allocation?
>1. You need the object to outlive the current scope
>    >That specific object at a specific memory location, not a copy of it 
>1. You need to allocate a lot of memory
>    >When we have to pay attention to our memory usage, large data structures are better kept on the heap (using pointers), because the stack could easily fill up (and we need to have space on the stack for local variables and keeping track of scopes)
>
>[Stack overflow: Why should I use a pointer?](https://stackoverflow.com/questions/22146094/why-should-i-use-a-pointer-rather-than-the-object-itself)

As mentioned above, if we want something to outlive the scope, we need pointers. Because if we create something within the scope of a function, a list perhaps, and want to return the list. This won't work if you didn't use pointers, because it would then live on the stack within the scope.

## Pointer Pointers!
```
int ** my_num;
```
This is a pointer just like before, it contains a memory address, but it will lead us to another pointer.

This lets you create a more flexable lists, where you don't actually have list of values, but a list of pointers, their values could be anywhere on the heap. 
>It saves you copying the values and moving them around, but instead you work with pointers, and leave the values wherever they landed in the heap (they aren't actually in linear space, but the list itself that holds the pointers to the individual values would be in linear space. Depending on the size of the actual values how much you save in processing by using pointer pointers)

## Pointer Conversion!
* [C-sharp corner: pointers](https://www.c-sharpcorner.com/article/pointers-in-C-Sharp/) 
> Since basic things in C seem to work throughout the different variations of the language
```cpp
char c = 'R';  
char *pc = &c;  
void *pv = pc; // Implicit conversion  
int *pi = (int *) pv; // Explicit conversion using casting operator  
```