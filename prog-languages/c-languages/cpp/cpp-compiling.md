# Combining programs in C++ 
If you are on a linux system, then the system would automatically include everything you need to compile a program from `c` or `c++` code. This does make linux systems very appealing for developing code in those languages.

it is called `g++` that we use to compile the code into an executable file

### On Windows
If you are on a windows machine, then you could install a _ubuntu terminal_ from the Windows store (if you have virtualization). It allows you to interact with your computer as if it were a linux system, which also means it will include the `g++`
> Though this does not actually allow you to generate executables that you could run from a windows GUI or terminal, since it would be compiling as if on a linux system.
>
>This is still a very vaid and quick way to compile the code, since the code itself won't care onto what system you compile it to. So this way you will know if you have errors.


If you actually want it to compile the executable so that it works on Windows, you will need to find a GNU compiler specially made for the windows system.

## GNU compiler (G++)
```
g++ -o <output-file-name> <input-file-name>
```
* This is the very basic way you could get `g++` to compile your code
* `-o` is a flag for the output (so that you can specify what the output file will be named, that the compiler will create)
* The last argument is the file that contains the code. 
* If you have multiple `cpp` files that the program is composed of, then you need to specify them all at the end.
    ```
    g++ -o <output-file-name> <input-file1-name> <input-file2-name> ...
    ```

### Makefile
> I think it comes with GNU, so shouldn't need to add anything if you got that.

To make your life easier when compiling, you can store these commands in a `Makefile`, which lets you easily run and rerun commands. Specially handy if there are multiple files involved and/or special flags

```
my_program: <list all files it should watch for changes>
    <the CLI command you want to run when using 'my_program'>
```
* Then you can simply use the following command to run things
* `my_program` is something called a _recipe_ within the makefile
```
make my_program
```

If you have many _recipes_ stored in the makefile, you can specify which ones you want to run from the base, if you simply use the command `make`, it will run whatever recipes you specified as the default in the file.

## Compiling program with multiple files
### Include the CPP file directly
> It is not reccomended to do this, because it will insert the code form the included `.cpp` file into your code. This is not good if we wanted to use the code from the included file in other files as well (which would become part of the whole program), since we can only define things once. 
>
>This would lead to circular references and issues.
>
>But it's good to know this can be done, 

1. First you need to have your main program file (where your `main()` is located), the file itself can be named anything. Only one file in our program can contain the `main()`

    #### `main.cpp`
    ```cpp
    #include <iostream>
    using namespace std;

    int main (){
        cout << my_max(8,7) << endl;
        return 0;
    }
    ```

1. Next you might define other functions in another file
    #### `my_max.cpp`
    ```cpp
    int my_max(int a, int b){
        if (a > b){
            return a;
        } else {
            return b;
        }
    }
    ```

    You could make as many secondary `.cpp` files

1. Then we will include `print_age.cpp` in `main.cpp`
    #### `my_programs.h`
    ```cpp
    #include <iostream>
    #include "my_max.cpp" //this is just another c++ file

    using namespace std;

    int main (){
        cout << my_max(8,7) << endl;
        return 0;
    }
    ```

### Using a Header File
You have a single file where you declare all the functions from the external files (the other `.cpp` files).

1. Again we have the same `my_max.cpp` file as in the previous section
1. This time we will create a `my_max.h` file, to store our definitions
    ```h
    int my_max(int a, int b);
    ```
    * There only needs to be one `.h` file that collects the declarations of what we defined in the extra files. So no need for making one for each `.cpp` file.
1. And we import it to the `main.cpp`
    ```cpp
    
    #include <iostream>
    #include "my_max.h" //this is a header file

    using namespace std;

    int main (){
        cout << my_max(8,7) << endl;
        return 0;
    }    
    ```
1. Then we compile all the files of the program __together__
    * `main.cpp`
    * `my_max.cpp`
    * ... all the `.cpp` files that are used in the program

    ```
    g++ -o maxer main.cpp my_max.cpp
    ```
    and now it will compile an executable named `maxer` that you can run!

    #### When not compiling with all the files
    >If we would only compile the `main.cpp`
    >```
    >g++ -o maxer main.cpp
    >```
    >then it will not know what to do when it comes to using `my_max()`, it only has the _declaration_, not the __definition__ (how we implemented it)
    >```
    >main.cpp:(.text+0x17): undefined reference to `my_max(int, int)'
    >collect2: error: ld returned 1 exit status
    >```
    >This is what we'd call a linking error, it failed to link the declaration to the definition

