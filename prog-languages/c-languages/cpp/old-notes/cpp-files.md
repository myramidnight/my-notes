# Opening and closing files in C++

The following code snipped will create a file `myfile.txt` and write into it `fopen example` before closing the file.

```cpp
//stdio = standard input output
#include <stdio.h> 
int main () {
    FILE * pFile;
    pFile = fopen ("myfile.txt","w");
    if (pFile != NULL)  {
        fputs ("fopen example", pFile);
        fclose(pFile);
    }
    return 0;
}
```