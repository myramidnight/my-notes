Everything in this directory is essentially notes that have yet to be _assimilated_ into the main notes. Because I took a course specially about C++, figured I would just put priority on those notes. 

* [G++ compiler for windows (and VSCode)](https://code.visualstudio.com/docs/cpp/config-mingw)

# `C++`
### C vs. C++ vs. C#
>`C` is a low-level programming language, and `C++` (cpp) extends it's functunality a bit, but not enough to pull it out of being a low-level language.
>
>`C#` (c-sharp) however improves the language so much that it's been labeled a high-level programming language. It has alot of overhead and libraries that need to be included before it will compile. The core of `.NET` is built with `C#`.


### Difference between `C` and `C++`
| `C` | `C++`
| --- | ---
| contains 32 keywords | contains 52 keywords
| supports procedural programming | is a hybrid language, because `C++` supports both procedural and object oriented programming paradigms 
| is function-driven language | is object-driven language

## Using C++
It is honestly a lot simpler to be coding C++ on a linux system, because usually everything you need is already included so you can compile it, or at least easier to install what you need with `apt-get`, than on a _Windows_ system.

I have been using a raspberry pi for programming in C++ lately, accessing it through _team-viewer_ on my main computer.

### Simple example of printing
```cpp
#include  <iostream> //linter gives error, works fine
using namespace std; 

int main() {
    std::cout << "Hello world!" << endl; 
    printf("Hello again!"); //inbuilt print function of c
}
```

* `cout` is very useful for printing output, because it has been _overloaded_ with many features, being able to detect the type you are trying to print so you don't always have to rememeber to convert things to strings before giving it to the printer.

### Compiling with GCC (GNU Compiler Collection) 
It is also called the `G++` compilor, and it handles compilation of both `C` and `C++`.
* You can optionally specify the programming language version with `std` before compling. An example of the compiling command is below. 
    ```
    g++ -std=c++11 project1.cpp -o myProject
    ```
    * The output file will be a `.exe`
    * It is important that the program has a `main` function before compiling, so it knows where to start.

### Managing memory
If we create variable within a scope, it gets deleted when we leave that scope, since the variables only live within that scope.

```cpp
string myVar = "String value"
```
We can however create pointers that will store our value somewhere in memory and our variable will simply contain a pointer to that memory location. This allows the value to live outside of the scope.

```cpp
MyClass * myPointer = new MyClass();
```
> This example would create a pointer for my custom class, using the `new` keyword. Now if you print out `myPointer` you would get a memory location rather than the class itself.

But when we create pointers, we have to handle deleting them as well, unless we want to have a _memory leak_ (the program using more memory than it needs until system simply has no more memory to provide and things crash/freeze)

```cpp
delete myPointer;
```

## C++ syntax detection in VSCode
This just provides visual aid to the programmer, by color coding the text based on syntax and underlining code that would cause errors and warnings. 

But it can be very distracting when this feature gives you the wrong impressions, such as underlining valid code. Perhaps the default setting it uses for syntax detection is not ideal for you, and time to manually assign it to something that works correctly:
* [Why do I see red squiggles under Standard Library types?](https://code.visualstudio.com/docs/cpp/faq-cpp#_why-do-i-see-red-squiggles-under-standard-library-types)
    > Just go into the VSCode settings and search for `intelliSenseMode` to locate this setting. Change it as suggested in the link above.
    >* `clang-x64` or `gcc-x64` on linux
    >* `msvc-x64` on windows