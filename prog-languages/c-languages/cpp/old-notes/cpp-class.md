# Classes in C++

This example of a class simply prints out a message when the instance is created, and when the instance is deleted. 

## The Manager class
```cpp
#include <iostream>
using namespace std;

//Recipe for our class
class Manager {
    private: //private variables
        int _myNumber; 
    public: //Public variables
        Manager(/* args*/);
        Manager(int myNumber);
        ~Manager();

        //class method
        int getMyNumber();
        void setMyNumber(int);
};

//Constructor
Manager::Manager(/* args*/){
    _myNumber = 1987; //initialize the class variable
    cout << "Manager was created" << endl;
}

Manager::Manager(int myNumber){
    _myNumber = myNumber;
    cout << "Manager was created, with number" << endl;
}

// Destructor
Manager::~Manager(){
    cout << "Manager was deleted" << endl;
}

//Creating the method in Manager
int Manager::getMyNumber(){
    return _myNumber;
}

void Manager::setMyNumber(int myNumber){ 
    _myNumber = myNumber;
}
```

### Including the class in the main program
```cpp
//Include our manager class
#include "Manager.cpp"

int main(int argc, char *argv[]) {
    //Create our manager within this scope
    Manager myMan = Manager(); 
    cout << myMan.getMyNumber() << endl;

    //Creating manager with inital number
    Manager myNumMan = Manager(354);
    cout << myNumMan.getMyNumber() << endl;
    
    //Creating class with a pointer (*)
    Manager * myMan2 = new Manager(); 
    cout << myMan2 << endl; //outputs location in memory
    myMan2->setMyNumber(888); //interacting with pointers has these arrows
    cout <<myMan2->getMyNumber() <<endl;
}
```