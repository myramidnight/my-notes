### Loop through std::list
```cpp
list<int> myList = {1,2,3,4};
//loop through the items in list
for (int number : myList ){
    cout << number << endl;
}
```