# Structures and Classes

Main difference between a struct and class initially is that
* properties in a struct are public by default
* properties in a class are private by default
    * we need to specially specify `public:` to make class properties public

## Struct
It is very useful to be able to define your own data structures, objects of your design.
```cpp
struct WordHolder{
    int length;     //length of word
    char* word;     //pointer to string
};

words = new WordHolder[8] //array of 8 WordHolders
```

## Class
* Constructor
* Copy constructor
    > Need to think about overloading the = operator
* Destructor
```cpp
class WordHolder{

    WordHolder(char* string){ //constructor that takes string
        length = strlen(string);
        word = new char[length];
    }

    int length;
    char* word;
}
```
```cpp
class WordHolder{
private:
    WordHolder(){ //default constructer
        length = 0;
        word = NULL;
    }

    WordHolder(char* string){ //constructor that takes string
        length = strlen(string);
        word = new char[length];
    }

    ~WordHolder(){ //destructor
        delete[] word;
    }
public:
    int length;
    char* word;
}
```

* Everything would be private by default, no need to specify it othervise.
* The default constructor is only required if we would want to populate something like an array, when you want it to generate, not having to specify every single item.
* class desctructor is something that will run when the instance is deleted, if the class happens to allocate memory, we use it to automatically deallocate
* `virtual` keyword is important for deconstructor, is relevant to inheridence (to avoid memory leaks)

### External Constructors and Destructors
It is just a different way to do things, its same as previous example (I just removed the code from the body, to keep things clear)
```cpp
class WordHolder{
    int length;
    char* word;
}

//Default constructur
WordHolder::WordHolder(){}

//Constructor that takes arguments
WordHolder::WordHolder(char* string){}

//Destructor
WordHolder::~WordHolder(){}
```