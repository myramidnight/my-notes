# Arrays
* Static arrays
    > Their size is set when defined, they do not change size through out it's lifetime. Memory is allocated for that array.
* Dynamic arrays
    > Interacting with these different types of arrays is identical, only difference happens to be how they are defined. These will have a pointer to a place in the heap, and their size is flexable because we are not pointing to their physical locations (the items in the array) but to pointers to where they are in memory.

## Arrays and pointers
Arrays are actually just pointers. They point at the first item in the array.
```cpp
int array[12];  //array of 12 integers
array[0] = 18;  //setting the value of first item to be 18
```
* If we would print out `array`, we'd get the memory address of the head of the array (the first item in the array)
* If we would print out `*array` (dereferenced pointer), you would get the `18` (the value of the first int in the array).
* We would of course get `18` if we printed out `array[0]`

The memory that the array takes would be continuous (each item in the array physically next to each other in order, on disk), which makes them very fast to navigate, you know where the array starts, you know how large the data type is (here it is `int`, 8 bytes), so you just jump from the head by `8*index` to find that item within the array.

>#### Linked lists aren't arrays
>Unless you would be using _linked lists_ specially, such data structures would keep pointers to where the next item in the list is located in memory, so a single list could be spread all over the memory, which makes it slower to navigate, because you have to iterate through every item to find the location of the next.

### Strings
A string is simply an array of `char`
```cpp
char myString[] = "Hello";
```
* This creates a char string of lenght `5` (it could be larger, if we specified the array size in the braces), because there is always one excape character at the end, to tell the computer that "the string ends here", even if it might not use the full size of the array.
> though there is a library that handles those char arrays in a similar way as you would expect a string to behave in higher level programming languages. Storing the array in an object type string.


## Static arrays
>Simply static because their size doesn't change, not that their item values might be static
```cpp
int my_array[8];
```
This array would be of 8 integers in size (and each int is 4, so it's 32 bytes), but we did only set asside memory to use the array, the program just assigned it some random place of unused memory, and it probably holds the bit values from when it was preivously used, so if you print out that array it would just give you 8 random values.
```cpp
int my_array[8] = {};
```
This is how we assign values to the array, within the curly braces. but if we give it a empty list in the curlies, then it defaults all the 8 spots with zero. If we only partially fill out the list, the remaining spots get defaulted to zero.

```cpp
// printing out the pointer to where the beginning of 'my_array' is located
cout << my_array << endl;
```
This is a way to print out someting, here we are trying to print the array, but this will just give us the pointer towards the first item in the array. We need a loop to print out the individual values
```cpp
// Print out each value in the array 'my_array'
for (int i = 0; i < 8; i++){
    cout << my_array[i] << endl;
}
```
```cpp
// This prints out value of 5th item in 'my_array'
cout << my_array[4] << endl;
```
It technically doesn't jump directly to the specified item in the array, but it knows where the head is located (pointer to `my_array`) and can calculate based on how large integers are where it should jump to find the specified item within the array.

This makes static arrays very fast, because they don't have to search for each item, just calculate where they are based on where the head of the array is. Computers are very quick to calculate things.
