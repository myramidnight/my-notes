# Pointers and Memory
Generally when we talk about _memory_, it is the RAM (random access memory), not the harddrives (those are usually referenced as being _disks_). But the memory is divided into two parts essentially, and both behave differently.

We store things in two ways:
* On the stack 
    > It is where the code of programs is kept while they are running. 
    * Stack is linear data structure.
    * Stack memory will never become fragmented
        >because when you store a list on stack, you specify a size, that size never changes. If it can change (using methods like append or remove), then it would be stored on the heap, where only a pointer will be stored on the stack, pointing to the memory address on the heap where the data can be found.
    * Stack accesses local variables only (live within the scope)
* On the heap (_kviklegt minni_)
    * Heap is a hierarchical data structure.
    * It is the _flexable_ memory
        >where things like lists can behave like it can change size during the course of the program 
    * Heap memory can become framgented as blocks of memory are first allocated and then freed.
    * Heap allows you to access variables globally 
        >like you have a key to a locker that stores things, and you can pass that key around, anyone with the key would have access to the locker

## Pointers and Variables
When we declare something in `C++`, we have to tell it explicitly what we're going to store within that variable, because it decides how much memory the program will allocate for it.

```cpp
int myInt; //an integer
int * myIntPointer; //A pointer to an integer 
```
When we add the `*`, we are telling the program "this will be a pointer", and if you would print the value of `myIntPointer`, then you would not get an integer, you would get a pointer to a specific place in the heap where the actual integer is being stored.

When you want to be a repsonsible programmer (specially in the lower level languages), then you should manage your memory. When you allocate memory on the heap, you should tell it when you're not using something anymore, to make your program more efficient (and prevent memory leaks, because when you run out of memory, your computer can't think anymore).

### Managing your memory (keeping things clean)
>#### Garbage collection
> Higher level programming languages pamper us, manging all aspects of memory, throwing it away when it's no longer in use (garbage collection). But the lower you go in the language hierarchy, the les    s the language will do for you, and in most cases you will have to really pay attention to what resources you are using.
>
>in short, we have to pick up our own trash in `C++`, if we don't want things to get messy. It doesn't stop using allocated memory on the heap just because we lost the pointer (that's what memory leak is).

Since pointers are _pointing_ to someplace on the __heap__, that piece of allocated memory will still be in use, even if the the variable that held the pointer might be outside of the scope (things no longer in scope are removed from the stack, as the program moves back to where the scope was started). 

```cpp
delete myIntPointer;
```
This would tell the computer "I'm not using whatever `myIntPointer` was pointing at", which will free that memory so it can be used by the computer for other things.

#### The stack and 'segmentation fault' errors
>If you try to delete something that happens to be on the stack, then you get an error, `segmentation fault`, because we can only remove or add things from the top of it, and we would be trying to delete something that is probably further into the stack. 
>
>You don't manually mess with the stack, it operates on a scope, whatever the program is currently doing. Whenever we enter a new scope, it adds what it needs for that scope, and when it leaves the scope, it will be back to the point where it was on the stack before it entered the scope. 
>
>Since functions are scopes, and they often return values, the computer will still hold onto that return value, and put that wherever you had specified the output of the function would go. That's how it doesn't get lost when the scope get thrown away. 

#### Pointing at nothing
>A pointer is simply something that will give us the memory address where a value is stored. The pointers themselves take space, they are a value themselves (their value being an address in memory), and if we delete whatever we were storing in the pointer, the pointer itself will still point to that location, but there isn't anything there anymore. 
>
>This can give you errors, if you try to use a pointer that points at a place that is no longer being used by the program.


### The values behind the pointers
Pointers are handy, they are like keys to a locker, and whoever has the key will know exactly where the locker is and have access to it (anything within the locker isn't actually locked, but that hardly matters if you don't know where the locker is).

But sometimes we don't want a key, we actually want what's within the locker.

#### Copying from pointers
>```cpp
>int myNewInt = * myIntPointer;
>```
>This will actually copy whatever values `myIntPointer` is pointing at, and storing it in `myNewInt`, which happens to be on the stack.

#### Accessing the value stored at the pointer address
>```cpp
>cout << myIntPointer << endl;   //Prints out the memory address
>cout << &myIntPointer << endl;  //Prints out the integer stored at that address
>```
>The `&` will allow us to access the value at the memory address stored in the pointer

### Interacting with classes on the heap
If we would store an instance of a class on the heap, then we actually interact differently with it than if it were on the stack.

```cpp
//The definition of 'MyClass'
class MyClass {
    public: 
        int myNum = 0;
};
```
> Got to remember the `;` after defining classes, it can only be skipped when defining functions
* Instance of class stored on stack
    ```cpp
    //Creating an instance of a 'MyClass' named 'myIntClass'
    MyClass myIntClass;

    //Changing the value of 'myNum' within the class
    myIntClass.myNum = 8;

    //Printing out the value
    std::cout << myIntClass.myNum << std::endl;
    ```
* Instance of class stored on heap
    ```cpp
    //Creating an instance of a 'MyClass' named 'myIntClass'
    MyClass * myIntClass;

    //Changing the value of 'myNum' within the class
    myIntClass -> myNum = 8;
    ```
    * This actually causes a "Segmentation fault (core dumped)" error, which is caused by accessing memory that _does not belong to you_
        >I need to examine that example better
    * Simplest way to rememember why we do this (besides for the fact that the other way doesn't work on pointers) is to think "pointer" `->`


## Pointers to pointers
Where a single star `*` indicates a pointer, double star `**` is a pointer to a pointer.
