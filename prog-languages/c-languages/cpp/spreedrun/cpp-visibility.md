# Visibility in C++
Visibility refers to how visibile certain members or methods of a class/struct are. Who can see them, who can use them. C++ has three types of visibility.
1. Private
    >Only this class and it's Friend can access Private attributes
1. Protected
    >Only this class and its subclass can access Protected attributes
1. Public
    >Anyone can access Public attributes
* Everything within a _class_ is __private__ by default
* everything within a _struct_ is __public__ by default

>Using visibility properly will increase maintainability and readability of your code, but it has no effect on performance. Anything set as _private_ indicates that those members shouldn't be accessed directly, it prevents something outside of the class from effecting it when it shouldnt.

## Friends
`friend` is actually a keyword which implies that they have access to _private_ members of the class.