# C++ 
> Just notes from watching a [playlist about C++ by _The Cherno_](https://www.youtube.com/watch?v=18c3MTX0PK0&list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb)

C++ is just C with added features to make it a bit easier to use. The C language family is quite large. C and C++ are considered low-level languages (the lowest level being the pure binary commands that operate the CPU).

It is strongly typed, every type needs to be defined.

## How C++ works (or C in general)
C languages need to be compiled before it can be run. 

1. We write code in a __source__ text file (`cpp`)
    > It is a language that we understand, that we can give the computer directions on what we want to happen.
1. The source code gets __compiled__ into binary object files
    > A interpreter will process the source code to figure out what we want the computer to do
    >* _Header files_ do not get compiled
1. The object files are combined with a __linker__ to form a single file
    > This final product would generally be an executable or library file.

## Interpreting the code
### The Main
Every C program will need a __entry point__, which is the __main()__ by default, which is the entry point of the program, where it starts. __It is required.__ 
> We can actually define a different entry point than _main_, but generally it will simply be _main_.

```cpp
//Preprocessor statements 
#include <iostream> 

//The start of the program
int main()
{
    //Simple cose that prints and then waits for input
    std::cout << "Hello World" << std::endl;
    std::cin.get();
}
```

* The _preprocessor_ statements get evaluated before the code within the file gets compiled. 
    >This will get all the code being included and puts it into the file essentially, before it proceeds to evaluate the code we made.
* We can use _namespaces_, which is like importing whatever statements exist within that namespace to be used directly.
    ```cpp
    using namespace std;
    //Now we could write 'cout << ... ' instead of 'std::cout << ... '
    ```
    >But we need to be careful if the namespaces contain statements with the same names, because then they would overlap with nothing to tell them appart
* After the _preprocessor_ statements have been evaluated, the rest of the code gets compiled  
    >Compiling is just a interpreter that turns our source code into _machine code_, something that the machine will understand. This will generally produce different code based on your System (Linux, Windows, Android...) and what you want (executable, library..)
* __Header files__ do not get compiled, they get _included_ via the _preprocessor_ statements. After their code has been 'inserted', it gets compiled along with the rest of the source file.
* Every _source file_ (`.cpp` for C++ language) get compiled individually into _object files_ (`.obj` or `.o`).
    >If we are compiling straight from source to executable, then these object files are just temporary. You can specify to compile to object file to keep them and link sepearately.
* The __linker__ will then combine the _object files_ into a single file (executable or library)

## C++ is very strict
* Every type has to be specified
* Every end-line `;` will need to be included when expected.
    > Higher-level languages tend to auto-add them, making them optional. Not the case for C++
* It does not care about whitespace
    >when coding in C++ and C, you could essentially write the whole code in a single line, as long as we have the syntax correct. Though we tend to space things out just to make it more readable.
    >
    >This gives us a lot of freedom of how our code will look


## Output
* Best way to see what's going on is to output something
* It is even better to have a special class that handles logging messages
    >To create something that takes the message and prints it, then you could sipmly adjust this class to disable debug outputs, instead of having to search through your entire code to find all the output lines.

## Declaring and Defining
Since order matters in the code, we only have access to code that has been defined before us within our scope.

### Declaration (saying it exists)
You can actually __declare__ a statement (telling the program that it exists) before we actually define it. This allows anything below it to use it even if they don't know what it contains.
```cpp
int myFunc(int arg1);
```

### Definition (saying what it does)

Then you can __define__ the statement later, which is telling us what the statement does.
```cpp
int myFunc(int arg1){
    std::cout << arg1 << std::endl;
}
```

Defining the statement works as a declaration on its own, but it is nice that we can declare it somewhere further up in the code.
> Why? Because if we have two statements that want to reference each other, if we only define them in order, the first one would not know the later exists. So we could declare the later statement before the first to let it know that it exists. 

## Header files
Because both the definition and declaration can only happen once in the code,  We can use __header files__ to contain such declarations to be included wherever we need them, and then define those statements somewhere else.  

### Header Guards
Header guards prevent code from being included multiple times and causing errors
* Using `#pragma once` in a _header file_ will tell the program to only include it once and ignore any later includes of it.
    > its the _newer_ way of doing headerguards, but the older way used conditionals that check if something has been defined already (a whole lot more code than just saying `#pragma once`)

## Function quantification
* ### Static
    >Basically, it will create unique addresses to the static function within each translation unit, so even if they have the same name, they are different entities.

    If we define something as `static`, it means that it is only defined for the current translation unit.
    * In that case, there will not be any linking errors for code that is unreachable
    * Including a static statement will make it not cause errors because for each file that includes it, each translation unit will have their own versions of the statement 
        >essentially it's not part of the program as a whole, only visible to the current object file being compiled
* ### Inline
    It is a powerful concept that is commonly used with classes. If a function is _inline_, the compiler places a copy of the code at each point where the _inline function_ is called at compile time.
* ### Virtual
    >This is something used within classes when defining a class method/function.
    * The _virtual_ keyword indicates that the inherited function
    * This only really works correctly if we are using pointers
        >Else it just behaves as if we didn't define it as _virtual_

## Errors
* _Linking errors_ start with `L` in their error code
    >It is failing to link together calls to their definitions. 
    * The decleration or call needs to match the definition (return type and arguments)
    * If a statement has been defined more than once, it causes an error (already defined/has a body)
    * Sometimes we can compile our files fine, but discover linking errors when combining them, if something has duplicate definitions that only cause problems when things are being tied together, but not in the stand-alone compilations.
    * Code can only be _included_ once, else it will be duplcated
* _Compile errors_ start with `C` in their error code
    >It is failing to compile the code (because of syntax, memory... )

## Syntax notes
* The code does not care about whitespace, only 
* Semicolon `;` indicates the end of a statement
    >If it is missing, then all the code following the missing semicolon will result in errors, as it wont be interpreted correcty
    * We do _not_ need to add `;` after function definitions or `include` statements.
    
## The heap and Stack
The memory is split into two types essentially
1. __The Stack__
    >Anything on the stack only lives while the scope lives. Anything stored on the stack does not need to be manually deleted, since it just gets discarded when we exit the scope.
1. __The Heap__
    >Anything created using the `new` keyword will be allocated on the __heap__. Anything on the _heap_ will live even if we exit the scope.
    >
    >Because of how things live on the heap, we need to specifically delete them when we're done with them. This is to prevent memory leaks.
    >
    >If something was not created with `new`, tnen we don't need to use `delete` on it.