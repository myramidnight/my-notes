# Enum in C++
It is a neat way to give ordered __integers__ variable names, and have their values generage themselves (incrementing from a given number)

```cpp
enum Example{
    A = 5, B, C
};
//A is 5, B is 6, C is 7...
```
* If you do not define any values for the Enum variables, then it begins on `0` by default
* The example above shows you can define values and any variables you name after it will just increment from the previous item in the order.
* It essentially equals you defining them individually as int variables
    ```cpp
    int A = 5;
    int B = 6;
    int C = 7;
    ```
    >Enum simply handles incrementing the values for you, it is tedious to manually adjust so many values and maintain them (if you change their order or add something in between?)
* You can specify the enum statement you created as a type for a variable
    >That restricts the value of the variable to the available values of the enum statement
