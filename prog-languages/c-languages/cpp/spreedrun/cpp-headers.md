# Header files in C++
Header files have the file extention `.h`, which works for C as well. 
* If you want to specify that it's a C++ header file, then you use `.hpp`

## What are Header files?
They contain code that we want to include somewhere in our program, or simply store declarations of statements.
* Their main purpose is to contain a common place for declaration
    >They can contain definitions as well
* We do not need to specify header files when compiling a program
    >Because they are being included, so the preprocessor will fetch their content for you anyway and insert it into the code where it was included.
* They are very important when we want to use some code in multiple `cpp` files
    >Since we can only declare and define any statement once, and defining them in every file might prevent compile errors, but there would be linking errors when trying to combine them.
    
    > But then we're including it in multiple places, which is duplicating. To prevent such issues we will use `#pragma once` or other header guards to prevent the contents of the header file from being included in subsequent `cpp` files by the compiler when linking
