# Functions in C++
```cpp
void myFunction(int arg){
    //Do something
}
```
* We need to specify what the function returns, `void` simply means it returns nothing.
* The arguments will also require typing of course, if any
* If it is a non-void function, then you need to return what it expects
* Only the `main` (entry point) function does not force you to return anything
    >Because when we exit the _main_ function, then we are exiting the program, which means there is no point in returning anything.

## Overloading a function
You can actually define multiple functions with the same name, as long as they take in different parameters. 

```cpp
void Print(int value){
    cout << value << endl;
}

void Print(float value){
    cout << value << endl;
}

void Print(string value){
    cout << value << endl;
}
```
>Those two functions can live in the same space, they are not considered identical because even though they have the same name, their parameters are different (which identifies them as unique). If you are passing an integer to the _Print_ function, then it will not be confused about which one we intend to use.

* You might want to look into __Templates__ for an alternative to this kind of repetative (and tedious) function overloading.
## Scope of a Function
It is useful to be aware of what a _scope_ is, when dealing with functions (or even classes). 
* A scope is an environment where variables live.
* The _global scope_ is the highest scope of your source code
    >Like saying it is the root of all your scopes
    >
    >Though technically the Operating system is even higher scope, but you cannot access it because it is outside of the scope of your code. 
* When we create a function, we are defining a new scope. 
    >Anything we define within the function only lives within that scope and cannot be accessed from outside of that scope. And it only exists while we are inside the function.
* Everything that is created for any scope will live on the stack
* Everything within the current scope gets discarded from the stack when we leave the scope.
    >That is why anything created within the scope cannot be used outside of that scope, unless you use the `new` keyword to allocate it on the heap.

## Passing by Reference or Value
We can call and pass things around by either value or reference.
* If we have an integer, it's value would be the number it holds.
* Reference is essentially a pointer, it references an existing variable.
    >* References are not new variables, they are new aliases for existing variables. 
    >* Unlike pointers, references cannot be changed to be a reference to something else later. 

```cpp
int main(){
    int a = 5; 
    int& ref = a;     //Reference to 'a'

    ref = 6; //We just changed 'a'
}
```
* The `&` here does not mean address, it is a reference, because it comes after a type.
* It is very useful for passing data to functions without actually copying the values.

>If we are _not_ specifying a pointer or reference when passing data to a function, then we are __passing by value__. 

* __Passing by value__ will __copy__ the value being passed into the function, and then the function will be working with that copy.
    >Then it does not effect the original value where it is stored. It is working with a __copy__ of the value. We could change it's value within the function and it would _not_ apply to the variable we might have passed into the function.
* __Passing by reference__ gives the function access to the actual variable, passing it the memory address of the value. 
    >So if we manipulate it within the function, it will effect the variable being passed in.
    * Using a pointer 
        ```cpp
        int a = 5;
        void myFunc(int* value){
            (*value)++; //dereferencing value to interact with the content
        };

        myFunc(&a); //passing the memory address of 'a'
        ```
        `value` is just a memory address. We need to dereference it to interact with it's contents (otherwise we would be incrementing the pointer, which will mess it up)
    * Using a Reference 
        ```cpp
        int a = 5;
        void myFunc(int& value){
            value++; //no dereferencing needed
        };

        myFunc(a); 
        ```
        This does the same as the pointer example, but looks a lot neater and no de-referencing needed. `value` becomes an alias for `a` being passed in


## Function Pointers
> It is a way to store a function in a variable

