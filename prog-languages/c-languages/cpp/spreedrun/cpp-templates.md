[Templates in C++ (video)](https://www.youtube.com/watch?v=I-hZkUa9mIs&list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb&index=54)
# Templates in C++
> Template basically gets the compiler to write code for you based on your usage
 
Good example of template usage is if we wanted to create a function that could be reused to do the same thing with various types. Since C++ is a strongly typed language, we can't just give it whatever and expect it to figure it out normally.

### Example using Function Overloading
```cpp
void Print(int value){
    cout << value << endl;
}

void Print(float value){
    cout << value << endl;
}

void Print(string value){
    cout << value << endl;
}
```

### Same example using templates
```cpp
template<typename T> 
void Print(T value){
    cout << value << endl;
}

int main(){
    Print(5);       //Implicit 
    Print(5.5f);
    Print("Hello");
    Print<int>(5);  //Explicit
}
```
We could have written `<class T>` instead of `<typename T>` for the same results, but it might give the impression that it only accepts class objects (which is not the case).

* The `template` keyword is what turns the _Print_ function into a template
* `T` can now stand for __any type__
    >We could have named it anything, but using T is very generic placeholder for _Type_
* We can call the function implicitly 
    >Then we do not specify what type we are passing in, the function will just pick that up from the argument.
* We can call the function explicitly
    >Then we specify what type we want the function to be accepting, which restricts what we can pass into it when we call it.
    
* The template creates the __function overloads__ for you
    >Generating the code for the function overload based on the template, replacing the placeholder types with intended types. 
* The template itself isn't actual existing code
    >The example _Print_ function does not exist __until__ we call it. The code for it gets _materialized_ when it is called based on the given type (from argument or explicitly stated)
    >
    >So the template function could be riddled with errors, which would not be an issue for the compiler as long as the function is never called.

## Templates with Classes
>We can actually specify datatypes, rather than having a `typename` placeholder for any type.

Here we are going to use a _integer_ template, which can be any integer, and it gets replaced during compile time with the intended value (that is specified when we create an instance of the class).
```cpp
template<int N>
class Array{
    private:
        int m_Array[N];
}

int main(){
    Array<5> array; //Creates an array of size 5 at compile time
}
```
Lets extend that, to allow the array to hold any type

```cpp
template<typename anyType, int N>
class Array{
    private:
        anyType m_Array[N];
}

int main(){
    Array<int, 5> array; //Integer array of size 5
}
```
This is basically what the __Standard library__ (`std`) is all about, it contains a lot of handy templates ready to use. We were essentially creating our own version of `std::array`

* So templates are defining what the compiler should do during compile time, rather than coding something that runs at runtime.