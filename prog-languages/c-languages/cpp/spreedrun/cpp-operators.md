# Operators in C++
Operators are essentially just functions that we can access with special characters or keywords. Such as `this.add(other)` becomes `this + other`

This does not just include mathematical operators.
* `*ptr` the pointer operator, or dereferencing
* `&ref` the reference operator
* `new` and `delete` are actually operators 
* ... many more

## Operator Overloading
> Overloading basically means we are going to give it a new meaning, create something new with it, or adding parameteres to it.

```cpp

class Vector2{
    float x,y;

    Vector2(float x, float y)
        : x(x), y(y) {}

    Vector2 Add(const Vector2& other) const{
        return Vector2(x + other.x, y + other.y);
    } //Addition defined without operator overloading

    Vector2 operator+(const Vector2& other) const{
        return Add(other);
    } //We've created a + overloaded operator
};

//Creating instances of vectors
Vector2 pointA(2,3);
Vector2 pointB(5,5);

//Then using our Add function and using the + operator
Vector2 result1 = pointA.Add(pointB);
Vector2 result2 = pointA + pointB; //using our created operator
```

### Overloading the `<<` 
We have gotten familar with the `std::cout` to print things to console, but how do we do that with the classes we've created? Well we will need to overload the `<<` operator

```cpp
//Creating an overloaded << opearator for stream
std::ostream& operator<<(std::ostream& stream, const Vector2& other){
    stream << other.x << ", " << other.y;
    return stream;
}

//Creating equals == operator
bool operator==(const vector2& other) const{
    return x == other.x && y == other.y;
}

//Creating a not equals operator
bool operator!=(const Vector2& other) const{
    return !(*this == other); //it uses our == operator
}
```