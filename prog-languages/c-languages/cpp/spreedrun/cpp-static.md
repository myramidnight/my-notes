[Static in C++ (with the Cherno)](https://www.youtube.com/watch?v=f3FVU-iwNuA&list=PLlrATfBNZ98dudnM48yfGUldqGD0S4FFb&index=21)

# Static in C++
The meaning of the `static` keyword depends on the context
1. When you use it __outside__ of a class (or struct)
1. When you use it __inside__ a class (or struct)

## Static variable (Private for translation unit)
>It will tell the linker that the _static_ statement will only be visible to the current translation unit (the current `cpp` file being compiled)

```cpp
static int myVar = 5;
```

Basically, defining it as `static` makes it __private__ for the current translation unit (`cpp` fle)
> so a identically named variable between two files will cause errors unless one of them is _static_. This also means that you cannot use the _static_ statement outside of the current file, even if you _include_ it.


Without the `static` keyword, it will be _global_ for the compilation. 

## Static in a class/struct (singleton variables)
>A static variable within a class is going to share memory with all instances of the class. Basically makes a _singleton_ of that variable (between all instances of the class/struct, there will only be a single instance of the _static_ variable).
* Static variables are no longer class members
    >so they cannot be initiated for an instance. They can however be changed through the instances, which changes it for the class.
* Static variables and functions within classes can be called without an instance of the class/struct 
    >because it is tied to the class rather than the instance itself.
    ```cpp
    class MyClass{
        static void Print(){
            std::cout << "Hello!" << std::endl;
        }
    };

    int main(){
        //Calling the print function without class instance
        MyClass::Print(); 
    }
    ```
* Static can only interact with other static members
    >Because non-static would be part of instances, and static isn't part of any instance
    >
    >You will need to pass it the instance you want it to interact with if you want it to work together.