# Variables in C++
We can store values in variables, and we have to define what type of value is being stored within that variable so that the program will know how much memory it needs to allocate for the variable.

```cpp
int myInt = 3;
```

* char, int, float, double, bool... 
    > These are all just numbers, characters are just numbers that is interpreted for printing letters.

## Pointers and Reference
* Pointers are simply a value that represents a memory location.
* They allow you to reference things that lives outside of your current scope 
* They generally point to values stored on the _heap_ 
    >While things that only live while current scope is alive are kept on the _stack_ (memory that you do not control). Once you leave the scope, anything we put on the stack gets discarded.
    * When we use the `new` keyword, we are creating something that will live on the _heap_ and is referenced to with a pointer.
        > We have to remember to delete things from the heap that we are no longer using, we can't rely on _garbage collectors_ in C and C++ to clean up after us. 

```cpp
int myVar; //This will live in current scope
int * myVarPointer; //This will live on the heap
int ** myDoublePointer; //This is a pointer to a pointer
```

One example I heard was that values might be contents of a storage locker, and pointers are simply a memo that reminds you where the locker is located (it isn't locked, but easily lost).

## External variables
We can declare a variable as `extern` to say "this variable exists in another translation unit", and the linker will then look up the definition in the other translation units. 
```cpp
extern int myVar;
```


## Initializing variables and memory
If we declare a variable without defining a value (initializing it), then it will allocate the memory you need and it's default value will just be the _junk_ that was left in the memory at that location.
* If you don't want weird random values, then you should initialize your variables by giving them a value.