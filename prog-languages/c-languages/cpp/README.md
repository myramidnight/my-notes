Taking a class in programming in C++, so I'll just keep it separate for now

# Programming in C++

## Index
* ### [Arrays](cpp-arrays.md)
    > Arrays are essentially pointers. They point at the first item in the array (the first item, can also be called the _head_ of the array). Though those arrays could be stored on the stack or heap, depending on how you declare it.
    * [Strings](cpp-strings.md)
        > Arrays of characters
* ### Class and Data structures
* ### [Pointers and Reference](cpp-pointers.md)

* ### [Memory (Stack and Heap)](cpp-memory.md)
    * More about pointers and the heap
    * garbage collection
    * [The `new` keyword](cpp-new.md)
        >one way to put stuff on the heap
* ### Compiling the code
    * [Header files](cpp-header-file.md)


# Other
* [CPP speedrun notes](speedrun/README.md)
    > Just a little link to a new section of notes I added.