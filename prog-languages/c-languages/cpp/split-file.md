# Defining functions
You can implement functions below any other functions (or the main) and still have it work, but only if you place the definition for that function above it 

> Have to consider the scope of a program, what can it see from where? 

```cpp

//Defining the function 'my_extra'
int my_extra(int a, int b);

//The main program using 'my_extra'
int main(){
    return my_extra(2,4);
}

//The implementation of 'my_extra'
// main doesn't see defined from here
int my_extra(int a, int b){
    if (a > b){
        return a;
    } else {
        return b;
    }
}

```

When compiling the program, it just cares for things being defined first, but

### Same name functions
You can actually define two different functions with the same name, if they return different types.

It will treat them as different functions, and know which one you want to use based on what type you specify with the use of a if statement you can use for define.

```cpp
//my defined constant, with a random number behind it just to ensure it's uniqe ness
//ifndef means 'if not defined'
#ifndef MY_MAX_8749841981615
#define MY_MAX_8749841981615

int my_max(int a, int b);

double my_max(int a, int b);

#endif
```

```cpp

```

## Defining a function within another file

### Splitting a C++ program between files
> Works for C as well, they are related after all.


You might compile the extra files specially, since it might include code that you want to use in multiple places


> "Undefined reference to 'name-of-function'" implies a linking error (since it is trying to use a function that is in another file)

