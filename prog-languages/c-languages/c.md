[back to programming language index](../README.md)
# Notes about C programming
`C` is a low-level programming language, which means that it is very close to the hardware and assembly. It seems to require more attention to memory usage than high-level languages (such as _python_ or _javascript_ for example), the programmer needs to think about how many bytes they are using and other details like that.

> You might have heard of `C++` or `C#` (or any other `C` something). Those are very similar because they build ontop of `C` and extend functionality. 

## Data types
When programming in C, you will always need to define the what type of data will be stored in the variable so it can treat it correctly.
```c
   //type variable_name = value
   unsigned int my_number = 87;
   char my_initial = 'S';
```
### custom type definition
You can actually create a custom data type by using `typedef` with the desired type and custom name to reference to. Saves you alot of time to use such _type variables_.
```c
   typedef unsigned int byte;
   byte my_num = 1024; //is unsigned int
```

## Variables
```c
//type variable_list;
int           i, j, k;
unsigned int  u;
char          c, ch;
float         f, salary;
double        d;
```

```c
//type variable_name = value;
extern int d = 3, f = 5;    // declaration of d and f. 
int d = 3, f = 5;           // definition and initializing d and f. 
byte z = 22;                // definition and initializes z. 
char x = 'x';               // the variable x has the value 'x'.
```

## Binary values
It helps alot to understand binary and hexadecimal when working with C, because all values are defined by their binary. It just depends on the context how this binary is precented, that is why you declair data types.

`Hexadecimal` values work with any data type because it is like witing the binary value directly, C will understand that it is a `hex` value when you add `0x` at the front of it. 
```c
char asciiChar = "<";
int intNum = 60;
int hexNum = 0x003C;
// All have the same binary value: 0000 0000 0011 1100
// So in this example, the following is true: intNum == hexNum == asciiChar
```

## Operators
Precedence of operators
1. `*`, `/`, `%` Multiplication, division and remainder
1. `+`, `-` Addition and subtraction
1. `<<`, `>>` bitwise left shift and right shift

## Printf()
```c
#include <stdio.h> 
// <stdio.h> lets you use 'printf()'

//Usage: printf(const char *format, ...)
int main () {
   int ch;

   for( ch = 75 ; ch <= 100; ch++ ) {
      printf("ASCII value = %d, Character = %c\n", ch , ch );
   }

   return(0);
}

//ASCII value = 75, Character = K
//ASCII value = 76, Character = L
//ASCII value = 77, Character = M
//ASCII value = 78, Character = N
//.... and so on until it reaches value 100
```

### Placeholders for data types
You need to use placeholders that specify the type of data it will be displayed. 

 `printf("the %s jumped over the %s, %d times", "cow", "moon", 2);`


```python
#Python equivalent
print("the {0} jumped over the {1}, {2} times".format("cow", "moon", str(2)))
```

* `%c` or `%`  character
* `%s`  string of characters
* `%u`  unsigned decimal integer
* `%x`  unsigned hexadecimal integer
* `%X`  unsigned hexadecimal integer (capital letters)
* `%f`  decimal floating point
* `%E` or `%e`  Mantissa / Exponent
* `%d` or `%i`  Signed decimal integer
* `%n`  nothing printed 
* `%p`  pointer address

## Arrays

```c
//type arrayName [ arraySize ];
int   array [4] = {1,2,3,4};
```
### Strings in C
Technically there is no such thing as a 'string' in C, just a collection of characters. 
```c
char  string [] = "Hello world!"; 
// The 'string' is 13 letters (0-12), 
//so the array is actually 14 chars when counting the null-terminator.
//char  string [14] = "Hello world!\0"; 
```
Without the `\0` (null-terminator) at the end of a string, the program will not know when to stop reading the data and will just continue to the next byte and next... giving seemingly corrupted output. 

## For loop
```c
int i;
for (i = 0; i < 10; i++) {
    printf("%d\n", i);
}
```