[back to language index](../README.md)

# Java
> Not to be confused with JavaScript

We used this heavily in the REIR course (Algorythms), and made use of online version of [Algorithms 4.th edition](https://algs4.cs.princeton.edu/home/). You can download and use their custom data structures called [Algs4](https://algs4.cs.princeton.edu/code/) to use in the on-site assignments.

## Client programs
We used `Intellij` for programming enviorment (free community version).

## Declare DataTypes
They need to be declaired for everything, even for data structures to tell it the DataType of the key and value in order to know how to handle it.
> By default you have to declare how large the arrays are so it can know how much space to take in memory for it. Some structures that you can import are more flexible by just resizing the array when needed (these would come from imported libraries at least)

```java
String stringVar = "Some text";
#the variable type is declaired first

```

### Arrays
```java
String[] stringArray = String[9];
//this creates an array of strings, 9 items long. 

int[][] array2D = int[3][4];
//this would be a 2D array of integers 3x4 in size. 
```

### Instances of data structure classes 
The instances in these examples are data structures from the `Algs4` library. 
```java
Set<int> mySet = Set();
//This imported data structure called Set will contain integers.

BST<int, string> binSearchTree = BST();
//this BST structure will have integer keys and string values.
```

## Classes and Methods
### Declaring the Return type
*  `void` means the method will return nothing
* else you tell it what type of data will be returned

### `Public` and `Private`
> We use the `public` or `private` declarations to announce that a variable, method or class is publicly accessable or just for private use within it's scope.

### Creating a constant variable with `final`
> We use `final` to create a constant variable that cannot be changed after it has been assigned a value.

## Code example
Here is an example of a class named `Car`. It has the classes `toString` and `addMiles` that can be called upon publicly through the class. 
```java
public class Car {

    //the class variables
    private String final licensePlate; 
    public Int milesCounter = 0;

    //the initializer for the class
    public Car(String licensePlate){ 
        this.licensePlate = licensePlate; //sets the class variable
    }

    //A method that will return a string
    public String toString(){
        return this.licensePlate;
    }

    //method that just does something without returning anything
    public void addMiles(int miles){
        //will increase the milesCounter by input amount
        this.milesCounter = this.milesCounter + miles;
    }
}
```
Using the example class
```Java
import location.Car; //import the car class from somewhere 

//Creates a instance of Car with license plate.
Car myCar = Car("AB123"); 

//Get the license plate from myCar and store as variable
String myLicensePlate = myCar.toString(); //now contains the string "AB123"

//Use addMiles method to increase the milesCounter
myCar.addMiles(8); //increases the milesCounter in myCar instance by 8

int currentMiles = myCar.milesCounter; //now contains 8
//can only directly access instance variables if they are public.

```

## Mutable datastructures
Some data structures let you decide what data-type is used for keys or what type of data the structure stores. There can be cases of complaints from the linter or errors when you forget to declair these data types (had cases of complaints of 'using them raw'). The solution would be to not only state the datatypes when you declare the variable, but also when you initiate the new instance.
```java
import Bag; //just sample data structure from package/library

Bag<Integer> varBag = new Bag();
//would become
Bag<Integer> varBag2 = new Bag<>();
//or even
Bag<Integer> varBag3 = new Bag<Integer>();
```

## Careful of Null errors
Always make sure that something cannot unexpectedly be a `null`. All possible cases of `null` have to be handled properly. Using simple if statements to check if something might be null tends to be enough.

## Typical base code
```java
public class MyClass:
    public  myClass(){}

    //accessable method outside of the class
    public void myMethod(){}

    //private inner class
    private class innerClass:
        public innerClass(){}

    private boolean isTrue = false; //priavet variable
    public boolean isAccessable = true; //accessable outside of class

```

## Loops
### Iterable data structures
```java 
import SET; //some data structure
import StdOut; //some print thing

//initiate the data structure
SET<String> structure = new SET();
//make a iterator of it
Iterator<String> iterableStructure = structure.iterator();

//now we can loop through it.
while(iterableStructure.hasNext()){
    StdOut.print(iterableStructure.next());
}

//same loop, without the Iterator
for (String currString : structure){
    StdOut.print(currString);
}
```