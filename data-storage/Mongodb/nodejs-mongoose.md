# Mongoose for your MongoDB
> For `NodeJS`

This is a tool for your project to interact with `mongodb`. It gives you the ability to create __schema__ that help regulate the shape of the data for the database. 

You access each collection through the schema model that represents it.


## Schema
```javascript
const { Schema,  } = require('mongoose').Schema;

module.exports = new Schema({
    title: {type: String, required: true},
    _artistId: {type: Schema.Types.ObjectId, required: true, ref: 'Artist'},
    date: {type: Date, required: true, default: Date.now()},
    images: {type: Array},
    description: {type: String},
    isAuctionItem: {type: Boolean, default: false},
});
```

You could import other schema and use them as values. This will validate if the object that you wish to upload through the schema still fits the rules, even the sub objects.

### Primary keys, Foreign keys and values
MongoDB is a No-SQL, which means it stores whole objects as documents with no strict rules about it's shape or connections to other documents.

__Mongoose__ gives you the power to specify what fields are required, which one is the primary key (indicated by the single `_` prefix in the key name), what type of value they hold and default values if you want. It even lets you simulate foreign keys with reference that checks if the ID exists in the referenced collection.

## Connection
Here is an example of a connection that. It stores the sensitive data in a `.env` file that can be created with `dotenv`. It will give a console message upon connection. This connection file could be called `db.js` to indicate that it is the database connection for the application. 

```javascript
const dbName = process.env.MONGO_DB;
const dbPass = process.env.MONGO_DB_PASS
const uriMongo = `mongodb+srv://stefania19:${dbPass}@hoops.ufxjn.gcp.mongodb.net/${dbName}`
const connection = mongoose.createConnection(uriMongo, { useNewUrlParser: true, useUnifiedTopology: true }, (err,client)=>{
    if (err){ throw new Error(err);}
    console.log('Connected to mongo db server')
});
```
### Models from schema
Through the `connection` method you would be able to connect the imported schemas to the database, creating models that are exported. These models are now a interface to as many collections as there are models. This example has the 
```javascript
//The schemas
const playerSchema = require('./schema/Player')
const basketballFieldSchema = require('./schema/BasketballField')
const pickupGameSchema = require('./schema/PickupGame')

//Exports
module.exports = {
    Player : connection.model('Player', playerSchema),
    BasketballField: connection.model('BasketballField', basketballFieldSchema),
    PickupGame: connection.model('PickupGame', pickupGameSchema)
}
```