# Data storage
### Data as Files
> Sometimes you just want to save things to a `.txt` or `.csv` to store your data while testing or doing simple projects.

## Query languages
There are many databases to choose from, and I will only make notes on what I have interacted with.
### SQL (Structured Query Language)
* MySQL
    * [MariaDB](./MariaDB.md)
* [PostgreSQL](./postgresql.md)
* NoSQL (_"Not only SQL"_)
    * [MongoDb](MongoDB.md)
    * [Firestore](Firestore.md)

> ![SQL query order](sql-query-logic.gif)

### Other query languages
* [GraphQL](./graphql.md)
    > GraphQL is a specification typically used for remote client-server communications.