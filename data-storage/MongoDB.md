# MongoDB
It is a `NoSQL` database

### Mongoose for NodeJS
If you are working with `NodeJS`, then you can check out the `mongoose` package to manage the connection between your application and the `MongoDB`. Because _NoSQL_ databases are very freeform, meaning there are no strict rules about the content unless you set them. _Mongoose_ allows you to create _schema_ to validate the data format (because inconsistent data can cause errors for the application using the data).