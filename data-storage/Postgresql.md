# PostgreSQL

* Has single storage engine
* Uses `PgAdmin` instead of `PhpMyAdmin` for GUI tool.
* Has a `SERIAL` id for auto increments.
> [PostgreSQL vs. MySQL](https://www.postgresqltutorial.com/postgresql-vs-mysql/)

The predicessor of Postgres is called _Ingres_, so the name _Postgres_ directly implies that it is post-ingres. Later it evolved into the PostgreSQL we know. 

> Ingres spawned a number of commercial database applications, including Mycrosoft SQL Server and others.

While _MySQL_ might be considered the most popular open source database since forever, _PostgreSQL_ has been dubbed the most __adbanced__ open source database and is steadily reaching the same popularity as _MySQL_.

The configuration of PostgreSQL can be confusing. Apparently postgreSQL isn't offically supported by Wordpress, which is very popular for basic websites and blogs. People have made plugins as a bridge, but it is little insurance that those plugins will be updated.

## Usage
> [PostgreSQL tutorial](https://www.postgresqltutorial.com/)
