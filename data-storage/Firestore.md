# Cloud Firestore
* A `NoSQL` database server
* This is part of Google's firebase service.

Google's documentation for the Firebase service is very detailed and easy to use. There are examples for the main programming languages, such as `NodeJS` and `Python`. But the implementation isn't universal, so each language actually has it's own implementation and some might have more features than others. `NodeJS` plugin probably has the most features implemented.