# Tips and Tricks
Just handy things to know are available when developing applications or websites.

## Programming
### Clean code
Simply refers to <u>code that is easy to read, understand and maintain</u>. It runs better in production, is better for colaborating in development and easier to debug. 

So use best practices and create clean code.

> Even if you don't expect anyone else to look at your code, but most likely someone will have to look at it after a few months and have no clue what they are working with, and that person will most likely be you.

* [The Top 5 Javascript issues in codebases](https://www.youtube.com/watch?v=5mZGsO_dG8o)

### Complexity of the code
Apparently cognative complexity of code tends to be high, making the code more difficult to understand. 
* _Cyclomatic complexity_ measures the number of paths through a function
* _Cognative complexity_ measures how easy it is to read a function
    > Breaks in the flow (loops and conditionals) and nesting 
> How many tests will you have to write for the function?
