# Git Archive
Awesome way to zip up your projects without all the files that `.gitignore` doesn't track. 

```
git archive -o this_repo.zip HEAD
```
> This will zip up the current commit (HEAD) 