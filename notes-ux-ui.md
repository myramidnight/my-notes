## User Experience
> From presentation:
>* The User Experience: Definition __ISO-9241 -210 standard (2019)__
>* "User’s perceptions and responses that result from the use _and/or anticipated use_ of a system, product or service. 
>
>NOTE 1: User’ perceptions and responses include the users’  emotions, beliefs, preferences, perceptions, comfort, behaviours and accomplishments that occur before, during and after use."
>* Satisfaction–Extent to which the user’s physical, cognitive and emotional responses that __result from the use of a system__, product or service meet the user’s needs and expectations

So user experience != satisfaction
### Measuring user experience
* Spurningarlistar, stjörnu/einnkunnargjöf. Bera saman fyrir og eftir notkun.