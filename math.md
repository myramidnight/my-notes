# Some math things for calculators
Though if it gets specific, then I'm using a __Cannon F-604 Scientific Statistical Calculator__. 
## Finding Modulo with calculator
If there will be no reminder, then the answer will always be 0

`52 mod 12`
1. First divide it normally, `52/12` = `4.33333...`
1. Subtract the whole number from the results `4.33333... - 4 = 0.33333...`
1. Then multiply that with the mod, `0.33333.. * 12 = 4`
1. the answer is `4` (the whole number if it happens to be a fraction)

#### another example where mod is larger
`5 mod 512`
1. We divide the larger number with the smaller `512/5 = 102.5`
1. then we multiply the resulting whole number (`102`) with the smaller number: `102*5 = 510`.
1. So the answer is `5` (how many times you could multiply it, to get close to the initial number)


## Finding the exponent with log (on scientific calc.)
How to find out what the exponent is in __2<sup>x</sup> = 512__ is?
*  __512 `log` / 2 `log` = 9__
* __2<sup>9</sup> = 512__